package interfaces;

import game.Bullet;
import game.Enemy;
import game.Game;
import game.Player;
import game.Weapon;
import game.Weapon.WeaponType;

import java.awt.geom.Point2D;
import java.nio.ByteBuffer;
import java.util.function.Function;

import core.CodeAdditions;
import core.RandomNumberGenerator;
import core.GameVars.PowerLevel;

public interface ISynchronisable {
	static final float maximumAimOffsetToSync = 5;// px

	byte[] getState();

	static boolean isFree(long ID)
	{
		if (ID == 0)
			return false;
		for (Object gO : Game._gameObjects) {
			if (gO instanceof ISynchronisable &&
					((ISynchronisable)gO).getID() == ID)
				return false;
		}
		return true;
	}

	static long getRandomtFreeID() {
		long lowestFreeID = 0;
		while (!isFree(lowestFreeID))
			lowestFreeID = RandomNumberGenerator.RNG.nextLong();
		return lowestFreeID;
	}

	@SuppressWarnings("unchecked")
	static <T extends ISynchronisable> T getByID(long id) {
		// id 0 stands for 'no object'
		if (id == 0)
			return null;
		// search for the object with this id and return it.
		for (Object obj : Game._gameObjects) {
			if (obj instanceof ISynchronisable) {
				if (((ISynchronisable) obj).getID() == id)
					return (T) obj;
			}
		}
		// if no object is found, return null.
		return null;
	}

	long getID();

	/**
	 * Thank java, as it doesn't allow the implementation of overridable static
	 * methods. We have now one Static method which has to determine which
	 * extension called it.
	 * 
	 * @param state
	 *            byte array containing data for the construction of the class
	 *            instance.
	 * @return One T object.
	 */
	@SuppressWarnings("unchecked")
	static <T extends ISynchronisable> T fromBuffer(ByteBuffer BB) {
		switch (BB.get()) {
		case 0:
			if (BB.remaining() + 2 >= new Player().getDataSize()) {
				Player temp = new Player(BB.getFloat(), BB.getFloat(),
						BB.getFloat(), BB.getFloat(), BB.get(), BB.get(),
						BB.get(), BB.get(), BB.get(), BB.get(), BB.getLong(),
						"");
				byte[] temp2 = new byte[Character.BYTES * 16];
				BB.get(temp2);
				temp._name = new String(temp2);
				return (T) temp;
			} else
				return null;
		case 1:
			if (BB.remaining() + 2 >= new Enemy((short) 1, PowerLevel.Thrash)
					.getDataSize())
				return (T) new Enemy(BB.getFloat(), BB.getFloat(),
						BB.getFloat(), BB.getShort(),
						PowerLevel.values()[BB.getInt()], BB.getLong(),
						BB.getLong());
			else
				return null;
		case 2:
			if (BB.remaining() + 2 >= new Weapon((short) 1, new Point2D.Float())
					.getDataSize())
				return (T) new Weapon(BB.getShort(), new Point2D.Float(
						BB.getFloat(), BB.getFloat()),
						WeaponType.values()[BB.getInt()], BB.getLong(),
						BB.getLong(), BB.getLong());
			else
				return null;
		case 3:
			if (BB.remaining() + 2 >= new Bullet(null, null).getDataSize())
				return (T) new Bullet(BB.getFloat(), BB.getFloat(),
						BB.getFloat(), BB.getLong(), BB.getLong(), BB.getLong());
			else
				return null;
		default:
			break;
		}
		return null;
	}

	int getDataSize();

	static <T extends ISynchronisable> int getDataSize(Class<T> DataType) {

		return 0;
	}

}

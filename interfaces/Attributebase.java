package interfaces;

import game.Bullet;
import game.Game;
import game.Player;
import game.Weapon;

import java.awt.Color;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import core.GameVars;
import core.GameVars.Attributenames;
import core.RandomNumberGenerator;

public abstract class Attributebase implements IRenderable, ISynchronisable{
	
	// Globals
	
	static final float baseHealth = 3;
	static final float baseMovementSpeed = 50f / (float)GameVars.getGTPS();
	static final float basePrecision = 0.1f;
	static final float baseMeleeDamage = 1;
	static final float baseMeleeRange = 30;
	
	public static final byte  baseAttributeValue = 1;
	public static final short baseAttributePoints = 10;
	static final byte  baseAttributePointsPerLevel = 2;
	
	// ->Shortcuts
	
	static final Attributenames _vita = Attributenames.Vitality;
	static final Attributenames _str = Attributenames.Strength;
	static final Attributenames _dex = Attributenames.Dexterity;
	static final Attributenames _agi = Attributenames.Agility;
	static final Attributenames _int = Attributenames.Intelligence;
	static final Attributenames _wis = Attributenames.Wisdom;
	
	// Protected Fields
	
	protected Point2D.Float _location;
	protected DrawType _drawType;
	protected Color _color;
	protected String _name;
	protected float _direction;
	protected int _attackCounter;
	
	public Weapon _weapon;
	protected GameVars.PowerLevel _power;
	
	protected float _health;
	protected float _maxHealth;
	protected float _precision;
	protected float _meleeDamage;
	protected float _meleeSpeed;
	protected float _meleeRange;
	protected float _movementSpeed;
	protected float _rangedDamageMultiplier;
	protected int _experience;
	protected int _experienceToLevelup;
	protected short _level;
	private long _id;
	
	// Internal Fields
	
	private byte _attributePoints;
	private Map<GameVars.EffectNames, Float> _effects = new HashMap<GameVars.EffectNames, Float>();
	
	// Pre-populated Fields
	
	private Map<GameVars.Attributenames, Byte> Attributes = new HashMap<GameVars.Attributenames,Byte>(){
		private static final long serialVersionUID = 6661488L;// yay sinnlos
	{
		for (GameVars.Attributenames An : GameVars.Attributenames.values()) {
			put(An,baseAttributeValue);
		}
		}};
	private float _size;
	
	// Getters
	
	public short getLevel(){
		return _level;
	}
	public float getHealth(){
		return _health;
	}
	public float getMaxHealth(){
		return _maxHealth;
	}
	public byte getVita(){
		return Attributes.get(_vita);
	}
	public byte getStr(){
		return Attributes.get(_str);
	}
	public byte getDex(){
		return Attributes.get(_dex);
	}
	public byte getAgi(){
		return Attributes.get(_agi);
	}
	public byte getInt(){
		return Attributes.get(_int);
	}
	public byte getWis(){
		return Attributes.get(_wis);
	}
	public int getExpLeft(){
		return _experienceToLevelup - _experience;
	}
	public int getExpToLevelup(){
		return _experienceToLevelup;
	}
	public int getExp(){
		return _experience;
	}
	public void setName(String value) {
		this._name = value;
	}
	public byte getAttributePoints(){
		return _attributePoints;
	}
	public void setAttribute(Attributenames attribute, byte value){
		Attributes.put(attribute, value);
	}
	public void setHealth(float value) {
		_health = value;
	}
	public void setLevel(short value){
		_level = value;
	}
	public Weapon getWeapon(){
		return _weapon;
	}
	public boolean hasWeapon(){
		return _weapon != null;
	}
	
	// Simple Methods
	/**
	 * A multiplier for health calculation.<br>
	 * <br>
	 * 10 * vita + <br>
	 * 3 * str + <br>
	 * 1 * dex + <br>
	 * 1 * agi + <br>
	 * 1 * int +<br>
	 * 1 * wis
	 * @return a multiplier based on the attributes.
	 */
	float healthMultiplier()
	{
		return
				getVita()	* 10 +
				getStr()	*  3 +
				getDex()	*  1 +
				getAgi()	*  1 +
				getInt()	*  1 +
				getWis()	*  1 / 17;
	}

	/**
	 * A multiplier for speed calculation.<br>
	 * <br>
	 * 1 * vita + <br>
	 * 1 * str + <br>
	 * 4 * dex + <br>
	 * 10 * agi + <br>
	 * 1 * int +<br>
	 * 1 * wis
	 * @return a multiplier based on the attributes.
	 */
	protected float speedMultiplier()
	{
		return  1 +
			   (getVita()	*  1 +
				getStr()	*  1 +
				getDex()	*  4 +
				getAgi()	* 10 +
				getInt()	*  1 +
				getWis()	*  1)/ 16;
	}

	/**
	 * A multiplier for melee-damage calculation.<br>
	 * <br>
	 * 1 * vita + <br>
	 * 10 * str + <br>
	 * 3 * dex + <br>
	 * 2 * agi + <br>
	 * 1 * int +<br>
	 * 1 * wis
	 * @return a multiplier based on the attributes.
	 */
	float meleeMultiplier()
	{
		return	1 +
			   (getVita()	*  1 +
				getStr()	* 10 +
				getDex()	*  3 +
				getAgi()	*  2 +
				getInt()	*  1 +
				getWis()	*  1)/ 18;
	}

	/**
	 * A multiplier for ranged precision calculation.<br>
	 * <br>
	 * 1 * vita + <br>
	 * 2 * str + <br>
	 * 10 * dex + <br>
	 * 2 * agi + <br>
	 * 2 * int +<br>
	 * 1 * wis
	 * @return a multiplier based on the attributes.
	 */
	float precisionMultiplier()
	{
		return  1 +
			   (getVita()	*  1 +
				getStr()	*  2 +
				getDex()	* 10 +
				getAgi()	*  2 +
				getInt()	*  2 +
				getWis()	*  1)/ 18;
	}

	/**
	 * A multiplier for ranged damage calculation.<br>
	 * <br>
	 * 1 * vita + <br>
	 * 2 * str + <br>
	 * 10 * dex + <br>
	 * 3 * agi + <br>
	 * 2 * int +<br>
	 * 2 * wis
	 * @return a multiplier based on the attributes.
	 */
	float rangedMultiplier(){
		return  1 +
			   (getVita()	*  1 +
				getStr()	*  2 +
				getDex()	* 10 +
				getAgi()	*  3 +
				getInt()	*  2 +
				getWis()	*  2)/ 20;
	}
	
	/**
	 * A multiplier for ranged damage calculation.<br>
	 * <br>
	 * 2 * vita + <br>
	 * 4 * str + <br>
	 * 4 * dex + <br>
	 * 10 * agi + <br>
	 * 1 * int +<br>
	 * 1 * wis
	 * @return a multiplier based on the attributes.
	 */
	float meleeSpeedMultiplier(){
		return  1 +
			   (getVita()	*  2 +
				getStr()	*  4 +
				getDex()	*  4 +
				getAgi()	* 10 +
				getInt()	*  1 +
				getWis()	*  1)/ 22;
	}
	
	//Complex Methods
	public void recalculate(){
		
		float powerMod;
		
		switch (_power) {
		case Furniture:
			powerMod = 0.01f;
			break;
		case Thrash:
			powerMod = 0.02f;
			break;
		case Common:
			powerMod = 0.05f;
			break;
		case Elite:
			powerMod = 0.2f;
			break;
		case Leader:
			powerMod = 0.6f;
			break;
		case Miniboss:
			powerMod = 1f;
			break;
		case Boss:
			powerMod = 3f;
			break;
		case Godlike:
			powerMod = 7f;
			break;
		case Player:
			powerMod = 1f;
			break;
		default:
			powerMod = 0f;
			break;
		}
		
		//TODO: check completion of initialisation.
		_meleeRange = /*rangeMultiplier * */ baseMeleeRange *powerMod;
		
		_maxHealth = healthMultiplier() * baseHealth * powerMod;
		_precision = precisionMultiplier() * basePrecision * powerMod;
		_meleeDamage = meleeMultiplier() * baseMeleeDamage * powerMod;
		_movementSpeed = speedMultiplier() * baseMovementSpeed;
		_rangedDamageMultiplier = rangedMultiplier() * powerMod;
	}
	
	protected void autoInitialize()
	{
		_level = 1;
		for(byte i = baseAttributePoints; i >= 0; i--)
		{
			addRandomAttribute();
		}
		recalculate();
	}
	
	void addRandomAttribute()
	{
		switch(RandomNumberGenerator.RNG.nextInt(6))
		{
		case 0:
			Attributes.put(GameVars.Attributenames.Vitality, (byte) (Attributes.get(_vita) + 1));
			break;
		case 1:
			Attributes.put(GameVars.Attributenames.Strength, (byte) (Attributes.get(_str) + 1));
			break;
		case 2:
			Attributes.put(GameVars.Attributenames.Dexterity, (byte) (Attributes.get(_dex) + 1));
			break;
		case 3:
			Attributes.put(GameVars.Attributenames.Agility, (byte) (Attributes.get(_agi) + 1));
			break;
		case 4:
			Attributes.put(GameVars.Attributenames.Intelligence, (byte) (Attributes.get(_int) + 1));
			break;
		case 5:
			Attributes.put(GameVars.Attributenames.Wisdom, (byte) (Attributes.get(_wis) + 1));
			break;
			default:
				new Throwable("Unknown Attribute-index.").printStackTrace();
		}
	}
	
	protected void autoLevelUp()
	{
		_level++;
		for(byte i = baseAttributePointsPerLevel; i >= 0; i--)
		{
			addRandomAttribute();
		}
		recalculate();
	}
	
	public void initialize()
	{
		_level = 1;
		_attributePoints += baseAttributePoints;
	}
	
	protected void levelUp()
	{
		_level++;
		_attributePoints += baseAttributePointsPerLevel;
	}
	
	public void Died(Attributebase murderer) {
		System.out.println(this.getName() + " was killed by " + murderer.getName() + ".");
	}

	@Override
	public Color getColor() {
		return _color;
	}
	
	@Override
	public float getDirection() {
		return _direction;
	}
	@Override
	public DrawType getDrawType() {
		return _drawType;
	}
	@Override
	public Point2D.Float getLocation() {
		return _location;
	}
	@Override
	public String getName() {
		return _name;
	}
	
	public boolean damageMe(Attributebase attacker, float rawdamage, boolean reflection) {
		
		float Totaldamage = rawdamage;
		if (_effects.containsKey(GameVars.EffectNames.Damage_Threshold))
			Totaldamage -= _effects.get(GameVars.EffectNames.Damage_Threshold);
		if (Totaldamage <= 0)
			return false;
		
		if (_effects.containsKey(GameVars.EffectNames.Damage_Absorption))
			Totaldamage *= 1f-_effects.get(GameVars.EffectNames.Damage_Absorption)/2;
		
		if (!reflection)
		{
			float returnDamage = 0;
			if (_effects.containsKey(GameVars.EffectNames.Spike_Damage))
				returnDamage = _effects.get(GameVars.EffectNames.Spike_Damage);
			
			if (_effects.containsKey(GameVars.EffectNames.Damage_Reflection))
				returnDamage += Totaldamage * _effects.get(GameVars.EffectNames.Damage_Reflection);
			
			if (returnDamage > 0)
				attacker.damageMe(this, returnDamage, true);
		}
		
		if (Totaldamage > _health)
		{
			float Power;
			_health = 0;
			Died(attacker);
			switch (_power) {
			case Furniture:
				Power = 0.5f;
				break;
			case Thrash:
				Power = 0.75f;
				break;
			case Common:
				Power = 1;
				break;
			case Elite:
				Power = 3;
				break;
			case Leader:
				Power = 5;
				break;
			case Miniboss:
				Power = 10;
				break;
			case Boss:
				Power = 40;
				break;
			case Godlike:
				Power = 100;
				break;
			case Player:
				Power = 7.5f;
				break;
			default:
				Power = 0;
				break;
			}
			if (this instanceof Player)
				attacker.GrantEXP((int) 
					Math.ceil(
							0.6 * 
							getLevel() *
							Math.min(
									attacker.getLevel() + 2,
									getLevel()) * 
							Power));
			return true;
		}
		else
			_health -= Totaldamage;
		return false;
	}
	
	private void GrantEXP(int value) {
		_experience += value;
		if (getExpLeft() <= 0)
		{
			levelUp();
			_experience = getExpLeft() *-1;
		}
	}

	@SuppressWarnings("unchecked")
	public static ArrayList<Attributebase> InRange(Point2D.Float location, float range, float dir, float spread)
	{
		float angle_top = dir + spread / 2;
		float angle_bottom = dir - spread / 2;
		float angle_target;
		
		
		Point2D.Float Target = new Point2D.Float(
				location.x + range * (float) Math.cos(dir),
				location.y + range * (float) Math.sin(dir));

		ArrayList<Attributebase> collection = new ArrayList<Attributebase>();
		for (Attributebase gO : IRenderable.Filter((ArrayList<IRenderable>)Game._gameObjects.clone(), Attributebase.class)) {
			
			angle_target = (float) Math.atan2(gO.getLocation().y - location.y, gO.getLocation().x - location.x);
			
			if (range+gO.getSize()/2 > gO.getLocation().distance(location) &&
					range+gO.getSize()/2 > gO.getLocation().distance(Target))
			{
				if (
					angle_target > angle_bottom && angle_target < angle_top
					||
					gO.getLocation().distance(location) <  gO.getSize()/2 *
					Math.abs(
							Math.tan(
								Math.PI / 2 +
								angle_bottom -
								angle_target))
					||
					gO.getLocation().distance(location) <  gO.getSize()/2 *
					Math.abs(
							Math.tan(
								Math.PI / 2 +
								angle_top -
								angle_target))
					)
					collection.add(gO);
			}
		}
		return collection;
	}
	
	public ArrayList<Attributebase> InRange(){
		return InRange(getLocation(), _meleeRange, getDirection(), _precision);
	}


	@Override
	public long getID() {
		return _id;
	}
	
	public void setID(long _id) {
		this._id = _id;
	}
	protected void attack() {
		if (_weapon != null)
		{
			while (_attackCounter > _weapon._attackSpeed * GameVars.getGTPS())
			{
				if (_weapon._pellets > 0)
					for (int i = 0; i< _weapon._pellets; i++)
						Game.addGameObject(
								new Bullet(
										this,
										_weapon));
				else
					for (Attributebase gO : _weapon.InRange()) {
						gO.damageMe(this, _weapon._damage, false);
					}
				_attackCounter -= _weapon._attackSpeed * GameVars.getGTPS();
			}
		}
		else
			if (_attackCounter > _meleeSpeed * GameVars.getGTPS())
			{
				for (Attributebase gO : InRange()) {
					gO.damageMe(this, _meleeDamage, false);
				}
				_attackCounter -= _meleeSpeed * GameVars.getGTPS();
			}
	}
	
	protected void attacktimer() {
		if ((_weapon != null && _attackCounter < _weapon._attackSpeed * GameVars.getGTPS())// Weapon isn't ready
			||//OR
			_attackCounter < _meleeSpeed * GameVars.getGTPS()) // Melee isn't ready
		{
				_attackCounter ++;
		}
		else if (_weapon != null && _attackCounter > _weapon._attackSpeed * GameVars.getGTPS())// Overcharged weapon
				_attackCounter = (int) (_weapon._attackSpeed * GameVars.getGTPS());
		else if(_attackCounter > _meleeSpeed * GameVars.getGTPS())// Overcharged melee
			_attackCounter = (int) (_meleeSpeed * GameVars.getGTPS());
	}
	
	@Override
	public float getSize() {
		return _size;
	}
}

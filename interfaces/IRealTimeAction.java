package interfaces;

public interface IRealTimeAction {
	/**
	 * Is called on every gameTick
	 * @param Environment the list containing all GaemObjects.
	 */
	void Tick();
}

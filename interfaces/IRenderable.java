package interfaces;

import game.Bullet;
import game.Game;
import game.Weapon;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.geom.Point2D;
import java.util.ArrayList;

import core.GameVars;

public interface IRenderable {
	boolean canBeRemoved();
	DrawType getDrawType();
	Color getColor();
	String getName();
	public void setName(String value); 
	Point2D.Float getLocation();
	default Point getOffsetLocation()
	{
		return new Point((int)getLocation().x + Game._viewOffset.x, (int)getLocation().y + Game._viewOffset.y);
	}
	public void setLocation(float x, float y);
	float getDirection();
	public void setDirection(float value);
	default float getSize(){ return 0; }
	default Point getSizeXY(){ return null; }
	
	@SuppressWarnings("unchecked")
	static <T extends IRenderable> ArrayList<T> Filter(ArrayList<IRenderable> list, Class<T> returnType)
	{
		ArrayList<T> result = new ArrayList<T>();
		for(IRenderable instance : list)
		{
			if (returnType.isInstance(instance))
				result.add((T) instance);
		}
		return result;
	}
	
	/**
	 * Use this method to render this GaemObject instance.
	 * @param Display The Graphics2D object to render this instance on.
	 */
	default void Draw(Graphics2D Display)
	{
		Color temp = Display.getColor();
		Display.setColor(getColor());
		switch(getDrawType())
		{
		case Circle:
			Display.drawOval(
					(int)getLocation().x,
					(int)getLocation().y,
					(int)getSize(),
					(int)getSize());
			break;
		case Line:
			Display.drawLine(
					(int)getLocation().x,
					(int)getLocation().y,
					(int)getSizeXY().x,
					(int)getSizeXY().y);
			break;
		case Point:
			Display.fillOval(
					(int)getLocation().x,
					(int)getLocation().y,
					(int)getSize(),
					(int)getSize());
			break;
		case Rectangle:
			Display.drawRect(
					(int)getLocation().x,
					(int)getLocation().y,
					(int)getLocation().x - (int)getSizeXY().x,
					(int)getLocation().y - (int)getSizeXY().y);
			break;
		}
		Display.setColor(temp);
	}
	
	enum DrawType
	{
		Line,
		Circle,
		Point,
		Rectangle
	}
	
	enum collision{
		/**
		 * Neither now colliding, nor colliding after moving.
		 */
		none,
		/**
		 * Not colliding right now, but will after moving.
		 */
		imminend,
		/**
		 * Currently colliding, with a way out.
		 */
		current,
		/**
		 * Currently colliding and no way out.
		 */
		stuck
	}
	

	default public collision getCollisionState(ArrayList<IRenderable> Environment)
	{
		collision col = collision.none;
		for (IRenderable gO : Filter(Environment, Attributebase.class))
		{
			switch (col) {
				case none:
					col = CollidesWith(gO);
					break;
				case imminend:
					if (	CollidesWith(gO) == collision.current ||
							CollidesWith(gO) == collision.stuck)
						return collision.stuck;
					col = collision.imminend;
					break;
				case current:
					if (	CollidesWith(gO) == collision.imminend ||
							CollidesWith(gO) == collision.stuck)
						return collision.stuck;
					col = collision.current;
					break;
				case stuck:
					return collision.stuck;
				default:
					break;
			}
		}
		return col;
	}

	default public collision CollidesWith(IRenderable gO)
	{
		// One cannot collide with itself.
		if (this == gO)
			return collision.none;
		
		collision col = collision.none;
		Point2D nextPos = /*(Point2D)getLocation().clone(); //*/new Point2D.Float();
		
		if (this instanceof Attributebase)
			nextPos.setLocation(
					getLocation().x + ((Attributebase)this)._movementSpeed / GameVars.getFPS() * Math.sin(getDirection()), 
					getLocation().y + ((Attributebase)this)._movementSpeed / GameVars.getFPS() * Math.cos(getDirection()));
		else
			nextPos = getLocation();
		
		if (gO.getLocation().distance(getLocation()) < getSize()/2 + gO.getSize()/2)
		{
			col = collision.current;
		}
		
		if (gO.getLocation().distance(nextPos) < getSize()/2 + gO.getSize()/2)
		{
			if (col == collision.current)
				col = collision.stuck;
			else
				col = collision.imminend;
		}
		
		return col;
	}
	
	default <T extends IRenderable> T ClosestOf(ArrayList<IRenderable> environment, Class<T> returnType)
	{
		T closest = null;
		for(T instance : Filter(environment, returnType))
		{
			if (instance != this)
			{
				if (closest == null)
				{
					closest = instance;
				}
				else if(instance.getLocation().distance(getLocation())-
						instance.getSize()/2-getSize()/2 <
						closest.getLocation().distance(getLocation())-
						closest.getSize()/2-getSize()/2)
					closest = instance;
			}
		}
		return closest;
	}
	
	default IRenderable Closest(ArrayList<IRenderable> environment) {
		IRenderable closest = null;
		for (IRenderable gO : environment)
		{			
			if (gO != this && !(gO instanceof Bullet) && !(gO instanceof Weapon))
			{
				if (closest == null)
					closest = gO;
				else if (	gO.getLocation().distance(getLocation())-gO.getSize()/2-getSize()/2 <
							closest.getLocation().distance(getLocation())-closest.getSize()/2-getSize()/2)
					closest = gO;
			}
		}
		return closest;
	}
}

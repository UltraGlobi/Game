package resources;

import java.net.URL;

public class ResourceLoader {
	private ResourceLoader(){} // This will be a static class. No constructor needed.
	
	public static URL loadResource(String resourceName)
	{
		return ResourceLoader.class.getResource(resourceName);
	}
}

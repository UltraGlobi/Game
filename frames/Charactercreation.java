package frames;

import game.Enemy;
import game.Game;
import game.Player;
import game.Weapon;
import game.Weapon.WeaponType;
import interfaces.Attributebase;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.geom.Point2D;
import java.io.IOException;
import java.net.Socket;

import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import core.CodeAdditions;
import core.GameVars.Attributenames;
import core.GameVars.PowerLevel;
import core.RandomNumberGenerator;

public class Charactercreation extends JFrame {
	private static final long serialVersionUID = -2826287777651435554L;
	
	Player customPlayer;
	
	GameWindow gw;
	
	int custompoints;
	
	JPanel p_Container;
	JPanel p_VitSelect;
	JPanel p_StrSelect;
	JPanel p_DexSelect;
	JPanel p_AgiSelect;
	JPanel p_IntSelect;
	JPanel p_WisSelect;
	JPanel p_LeftCol;
	JPanel p_RightCol;
	
	JButton b_Accept;
	JButton b_Cancel;
	JButton b_Vitincr;
	JButton b_Vitdecr;
	JButton b_Strincr;
	JButton b_Strdecr;
	JButton b_Dexincr;
	JButton b_Dexdecr;
	JButton b_Agiincr;
	JButton b_Agidecr;
	JButton b_Intincr;
	JButton b_Intdecr;
	JButton b_Wisincr;
	JButton b_Wisdecr;
	
	JTextField tf_Name;
	JTextField tf_Vitality;
	JTextField tf_Strength;
	JTextField tf_Dexterity;
	JTextField tf_Agility;
	JTextField tf_Intelligence;
	JTextField tf_Wisdom;
	
	JLabel l_Name;
	JLabel l_Vitality;
	JLabel l_Strength;
	JLabel l_Dexterity;
	JLabel l_Agility;
	JLabel l_Intelligence;
	JLabel l_Wisdom;
	
	public Charactercreation(GameWindow gw) {
		this.gw = gw;
		
		custompoints = Attributebase.baseAttributePoints;
		
		setSize(300, 200);
		
		p_Container = new JPanel(new BorderLayout());
		p_LeftCol = new JPanel(new GridLayout(0,1));
		p_RightCol = new JPanel(new GridLayout(0,1)); 
		p_VitSelect = new JPanel(new BorderLayout());
		p_StrSelect = new JPanel(new BorderLayout());
		p_DexSelect = new JPanel(new BorderLayout());
		p_AgiSelect = new JPanel(new BorderLayout());
		p_IntSelect = new JPanel(new BorderLayout());
		p_WisSelect = new JPanel(new BorderLayout());
		
		b_Accept = new JButton(actionHandler);
		b_Cancel = new JButton(actionHandler);
		b_Vitincr = new JButton(actionHandler);
		b_Vitdecr = new JButton(actionHandler);
		b_Strincr = new JButton(actionHandler);
		b_Strdecr = new JButton(actionHandler);
		b_Dexincr = new JButton(actionHandler);
		b_Dexdecr = new JButton(actionHandler);
		b_Agiincr = new JButton(actionHandler);
		b_Agidecr = new JButton(actionHandler);
		b_Intincr = new JButton(actionHandler);
		b_Intdecr = new JButton(actionHandler);
		b_Wisincr = new JButton(actionHandler);
		b_Wisdecr = new JButton(actionHandler);
		
		b_Accept.setText("Accept");
		b_Cancel.setText("Cancel");
		b_Vitincr.setText("Add Vit");
		b_Vitdecr.setText("Rem Vit");
		b_Strincr.setText("Add Str");
		b_Strdecr.setText("Rem Str");
		b_Dexincr.setText("Add Dex");
		b_Dexdecr.setText("Rem Dex");
		b_Agiincr.setText("Add Agi");
		b_Agidecr.setText("Rem Agi");
		b_Intincr.setText("Add Int");
		b_Intdecr.setText("Rem Int");
		b_Wisincr.setText("Add Wis");
		b_Wisdecr.setText("Rem Wis");
		
		tf_Name = new JTextField("Beginner");
		tf_Vitality = new JTextField(String.valueOf(Attributebase.baseAttributeValue));
		tf_Strength = new JTextField(String.valueOf(Attributebase.baseAttributeValue));
		tf_Dexterity = new JTextField(String.valueOf(Attributebase.baseAttributeValue));
		tf_Agility = new JTextField(String.valueOf(Attributebase.baseAttributeValue));
		tf_Intelligence = new JTextField(String.valueOf(Attributebase.baseAttributeValue));
		tf_Wisdom = new JTextField(String.valueOf(Attributebase.baseAttributeValue));

		tf_Vitality.setEnabled(false);
		tf_Strength.setEnabled(false);
		tf_Dexterity.setEnabled(false);
		tf_Agility.setEnabled(false);
		tf_Intelligence.setEnabled(false);
		tf_Wisdom.setEnabled(false);

		l_Name = new JLabel("Your Name: ");
		l_Vitality = new JLabel("Vitality: ");
		l_Strength = new JLabel("Strength: ");
		l_Dexterity = new JLabel("Dexterity: ");
		l_Agility = new JLabel("Agility: ");
		l_Intelligence = new JLabel("Intelligence: ");
		l_Wisdom = new JLabel("Wisdom: ");
		
		l_Name.setLabelFor(tf_Name);
		l_Vitality.setLabelFor(tf_Vitality);
		l_Strength.setLabelFor(tf_Strength);
		l_Dexterity.setLabelFor(tf_Dexterity);
		l_Agility.setLabelFor(tf_Agility);
		l_Intelligence.setLabelFor(tf_Intelligence);
		l_Wisdom.setLabelFor(tf_Wisdom);

		p_VitSelect.add(tf_Vitality, BorderLayout.CENTER);
		p_VitSelect.add(b_Vitincr, BorderLayout.EAST);
		p_VitSelect.add(b_Vitdecr, BorderLayout.WEST);
		
		p_StrSelect.add(tf_Strength, BorderLayout.CENTER);
		p_StrSelect.add(b_Strincr, BorderLayout.EAST);
		p_StrSelect.add(b_Strdecr, BorderLayout.WEST);
		
		p_DexSelect.add(tf_Dexterity, BorderLayout.CENTER);
		p_DexSelect.add(b_Dexincr, BorderLayout.EAST);
		p_DexSelect.add(b_Dexdecr, BorderLayout.WEST);
		
		p_AgiSelect.add(tf_Agility, BorderLayout.CENTER);
		p_AgiSelect.add(b_Agiincr, BorderLayout.EAST);
		p_AgiSelect.add(b_Agidecr, BorderLayout.WEST);
		
		p_IntSelect.add(tf_Intelligence, BorderLayout.CENTER);
		p_IntSelect.add(b_Intincr, BorderLayout.EAST);
		p_IntSelect.add(b_Intdecr, BorderLayout.WEST);
		
		p_WisSelect.add(tf_Wisdom, BorderLayout.CENTER);
		p_WisSelect.add(b_Wisincr, BorderLayout.EAST);
		p_WisSelect.add(b_Wisdecr, BorderLayout.WEST);

		p_Container.add(p_LeftCol, BorderLayout.WEST);
		p_Container.add(p_RightCol, BorderLayout.CENTER);
		
		p_LeftCol.add(l_Name);
		p_LeftCol.add(l_Vitality);
		p_LeftCol.add(l_Strength);
		p_LeftCol.add(l_Dexterity);
		p_LeftCol.add(l_Agility);
		p_LeftCol.add(l_Intelligence);
		p_LeftCol.add(l_Wisdom);
		p_LeftCol.add(b_Cancel);
		
		p_RightCol.add(tf_Name);
		p_RightCol.add(p_VitSelect);
		p_RightCol.add(p_StrSelect);
		p_RightCol.add(p_DexSelect);
		p_RightCol.add(p_AgiSelect);
		p_RightCol.add(p_IntSelect);
		p_RightCol.add(p_WisSelect);
		p_RightCol.add(b_Accept);
		
		add(p_Container);
		
		setVisible(true);
	}
	
	AbstractAction actionHandler = new AbstractAction() {
		private static final long serialVersionUID = 721424201516177053L;

		@Override
		public void actionPerformed(ActionEvent e) {
			switch (e.getActionCommand()) {
			case "Accept":
				Charactercreation.this.setVisible(false);
				Game.Initialize(gw._display);
				gw.setVisible(true);
				customPlayer = new Player();
				customPlayer.setID(RandomNumberGenerator.RNG.nextLong());
				customPlayer.initialize();
				customPlayer.setName(tf_Name.getText());
				customPlayer.setAttribute(
						Attributenames.Vitality,
						Byte.parseByte(tf_Vitality.getText()));
				customPlayer.setAttribute(
						Attributenames.Strength,
						Byte.parseByte(tf_Strength.getText()));
				customPlayer.setAttribute(
						Attributenames.Dexterity,
						Byte.parseByte(tf_Dexterity.getText()));
				customPlayer.setAttribute(
						Attributenames.Agility,
						Byte.parseByte(tf_Agility.getText()));
				customPlayer.setAttribute(
						Attributenames.Intelligence,
						Byte.parseByte(tf_Intelligence.getText()));
				customPlayer.setAttribute(
						Attributenames.Wisdom,
						Byte.parseByte(tf_Wisdom.getText()));
				
				customPlayer.recalculate();
				customPlayer.setHealth(customPlayer.getMaxHealth());
				if (GameWindow._isMultiplayer)
				{
					try {
						Socket _host = GameWindow._hostSocket;
						
						customPlayer.setID(Game._syncThread.getNewID());
						
						Game.addGameObject(customPlayer);
						
						Game._Player = customPlayer;
						
						Weapon starterweapon = new Weapon(customPlayer.getLevel(), null);
						
						starterweapon._id = Game._syncThread.getNewID();
						
						Game.addGameObject(starterweapon);
						
						starterweapon.setOwner(customPlayer);
						
						_host.getOutputStream().write(
								CodeAdditions.concatCommand(
										GameWindow._con_objectAdded,
										customPlayer.getState()));

						_host.getOutputStream().write(
								CodeAdditions.concatCommand(
										GameWindow._con_objectAdded,
										starterweapon.getState()));
						
						_host.getOutputStream().write(
								GameWindow._con_resyncStatement);
					} catch (IOException | InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
				else
				{
					Game.addGameObject(customPlayer);
					Game._Player = customPlayer;
				}
				
				Game.gameStart();
				break;
			case "Add Vit":
				if (custompoints > 0) {
					Charactercreation.this.tf_Vitality.setText(
							String.valueOf(
									Integer.parseInt(Charactercreation.this.tf_Vitality.getText())
									+ 1));
					custompoints--;
				}
				break;
			case "Add Str":
				if (custompoints > 0) {
					Charactercreation.this.tf_Strength.setText(
							String.valueOf(
									Integer.parseInt(Charactercreation.this.tf_Strength.getText())
									+ 1));
					custompoints--;
				}
				break;
			case "Add Dex":
				if (custompoints > 0) {
					Charactercreation.this.tf_Dexterity.setText(
							String.valueOf(
									Integer.parseInt(Charactercreation.this.tf_Dexterity.getText())
									+ 1));
					custompoints--;
				}
				break;
			case "Add Agi":
				if (custompoints > 0) {
					Charactercreation.this.tf_Agility.setText(
							String.valueOf(
									Integer.parseInt(Charactercreation.this.tf_Agility.getText())
									+ 1));
					custompoints--;
				}
				break;
			case "Add Int":
				if (custompoints > 0) {
					Charactercreation.this.tf_Intelligence.setText(
							String.valueOf(
									Integer.parseInt(Charactercreation.this.tf_Intelligence.getText())
									+ 1));
					custompoints--;
				}
				break;
			case "Add Wis":
				if (custompoints > 0) {
					Charactercreation.this.tf_Wisdom.setText(
							String.valueOf(
									Integer.parseInt(Charactercreation.this.tf_Wisdom.getText())
									+ 1));
					custompoints--;
				}
				break;
			case "Rem Vit":
				if (Integer.parseInt(Charactercreation.this.tf_Vitality.getText()) > 1) {
					Charactercreation.this.tf_Vitality.setText(
							String.valueOf(
									Integer.parseInt(Charactercreation.this.tf_Vitality.getText())
									- 1));
					custompoints++;
				}
				break;
			case "Rem Str":
				if (Integer.parseInt(Charactercreation.this.tf_Strength.getText()) > 1) {
					Charactercreation.this.tf_Strength.setText(
							String.valueOf(
									Integer.parseInt(Charactercreation.this.tf_Strength.getText())
									- 1));
					custompoints++;
				}
				break;
			case "Rem Dex":
				if (Integer.parseInt(Charactercreation.this.tf_Dexterity.getText()) > 1) {
					Charactercreation.this.tf_Dexterity.setText(
							String.valueOf(
									Integer.parseInt(Charactercreation.this.tf_Dexterity.getText())
									- 1));
					custompoints++;
				}
				break;
			case "Rem Agi":
				if (Integer.parseInt(Charactercreation.this.tf_Agility.getText()) > 1) {
					Charactercreation.this.tf_Agility.setText(
							String.valueOf(
									Integer.parseInt(Charactercreation.this.tf_Agility.getText())
									- 1));
					custompoints++;
				}
				break;
			case "Rem Int":
				if (Integer.parseInt(Charactercreation.this.tf_Intelligence.getText()) > 1) {
					Charactercreation.this.tf_Intelligence.setText(
							String.valueOf(
									Integer.parseInt(Charactercreation.this.tf_Intelligence.getText())
									- 1));
					custompoints++;
				}
				break;
			case "Rem Wis":
				if (Integer.parseInt(Charactercreation.this.tf_Wisdom.getText()) > 1) {
					Charactercreation.this.tf_Wisdom.setText(
							String.valueOf(
									Integer.parseInt(Charactercreation.this.tf_Wisdom.getText())
									- 1));
					custompoints++;
				}
				break;
			case "Cancel":
			default:
				Charactercreation.this.setVisible(false);
				break;
			}
		}
	};
}

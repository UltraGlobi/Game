package frames;

import game.Game;
import game.UserInterface;
import game.UserInterface.UserInterfaceType;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.io.*;
import java.net.Inet4Address;
import java.net.UnknownHostException;
import java.security.cert.PKIXRevocationChecker.Option;
import java.util.Properties;

import javafx.scene.layout.Border;

import javax.swing.*;
import javax.swing.text.*;

import jdk.nashorn.internal.runtime.options.Options;
import resources.ResourceLoader;

public class GameMenu extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4991479057780942771L;
	Properties prop = new Properties();
	FileOutputStream fos;
	JPanel p_Main;
	JPanel p_Settings;
	JPanel p_Game;
	JPanel p_Graphics;
	JPanel p_Controls;
	JPanel p_Sound;
	JButton b_Game;
	JButton b_Graphics;
	JButton b_Controls;
	JButton b_Sound;
	JButton b_StartGame;
	JButton b_CloseFrame;
	JButton b_Exit;
	Color bfColor = new Color(40,80,250);
	Color bbColor = new Color(50,50,50);

	//Settings Controls
	JLabel l_fwd = new JLabel();
	JTextField t_fwd = new JTextField();
	JLabel l_bwd = new JLabel();
	JTextField t_bwd = new JTextField();
	JLabel l_lwd = new JLabel();
	JTextField t_lwd = new JTextField();
	JLabel l_rwd = new JLabel();
	JTextField t_rwd = new JTextField();

	
	public GameMenu() throws FileNotFoundException, IOException {
		//loadSettings();
		setContentPane((new JLabel(new ImageIcon(ResourceLoader.loadResource("background_01.png")))));
		setLayout(new FlowLayout());
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setSize(475, 370);
		setResizable(false);
		setLocationRelativeTo(null);
		
		b_CloseFrame = new JButton(pressButton);
		b_CloseFrame.setText("Zur�ck");
		setControl(b_CloseFrame, 'B');
		b_CloseFrame.setForeground(new Color(250,80,40));
		
		b_Game = new JButton(pressButton);
		b_Game.setText("Spiel");
		setControl(b_Game, 'B');
		
		
		b_Graphics = new JButton(pressButton);
		b_Graphics.setText("Grafik");
		setControl(b_Graphics, 'B');
		
		b_Controls = new JButton(pressButton);
		b_Controls.setText("Steuerung");
		setControl(b_Controls, 'B');

		b_Sound = new JButton(pressButton);
		b_Sound.setText("Audio");
		setControl(b_Sound, 'B');
		
		b_StartGame = new JButton(pressButton);
		b_StartGame.setText("Spiel starten");
		setControl(b_StartGame, 'B');
		b_StartGame.setForeground(new Color(250,80,40));
		
		b_Exit = new JButton(pressButton);
		b_Exit.setText("Exit");
		setControl(b_Exit, 'B');
		b_Exit.setForeground(new Color(250,80,40));
		
		p_Main = new JPanel(new BorderLayout());
		p_Settings = new JPanel();
		p_Main.setBackground(new Color(0, 0, 0, 0));
		p_Settings.setBackground( new Color(0, 0, 0, 100));
		p_Settings.setLayout(new BoxLayout(p_Settings, BoxLayout.Y_AXIS));
		p_Settings.add(Box.createVerticalStrut(20));
		p_Settings.add(b_StartGame);
		p_Settings.add(Box.createVerticalStrut(60));
		p_Settings.add(b_Game);
		p_Settings.add(Box.createVerticalStrut(5));
		p_Settings.add(b_Graphics);
		p_Settings.add(Box.createVerticalStrut(5));
		p_Settings.add(b_Controls);
		p_Settings.add(Box.createVerticalStrut(5));
		p_Settings.add(b_Sound);
		p_Settings.add(Box.createVerticalStrut(50));
		p_Settings.add(b_Exit);
		
		p_Main.add(p_Settings, BorderLayout.NORTH);
		add(p_Main);
		setVisible(true);
	}
	
	private void setControl(Object obj, char Controltype)
	{
		switch(Controltype){
			case 'B':
				JButton btn = (JButton)obj;
				btn.setSize(new Dimension(120, 24));
				btn.setMaximumSize(new Dimension(120, 24));
				btn.setForeground(bfColor);
				btn.setBackground(bbColor);
				break;
			case 'T':
				JTextField tf = (JTextField)obj;
				tf.setSize(new Dimension(120, 20));
				tf.setMaximumSize(new Dimension(200, 24));
				tf.setBackground(Color.black);
				tf.setForeground(Color.red);
				tf.setBorder(BorderFactory.createLineBorder(bfColor));
				break;
			case 'L':
				JLabel lbl = (JLabel)obj;
				lbl.setForeground(bfColor);
				break;
		}
	}
	
	AbstractAction pressButton = new AbstractAction() {
		
		private static final long serialVersionUID = -745974447979178323L;
		@Override
		public void actionPerformed(ActionEvent evt) {
			try {
				Object obj = evt.getSource();
				p_Main.remove(p_Settings);
				if(obj == b_StartGame){
					GameWindow GW = new GameWindow();
					UserInterface startMenu = new UserInterface(UserInterfaceType.StartMenu);
					startMenu.setActive(true);
					Game.addGameObject(startMenu);
					setVisible(false);
				}else if(obj == b_Game){
					setGame();
				}else if(obj == b_Graphics){
					setGraphics();
				}else if(obj == b_Controls){
					setControls();
				}else if(obj == b_Sound){
					setSound();
				}else if(obj == b_CloseFrame){
					saveSettings();
					p_Main.removeAll();
					p_Main.add(p_Settings);
					invalidate();
					validate();
				}else if(obj == b_Exit){
					dispose();
				}
				invalidate();
				validate();
			
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		};
	};
	
	private void setGame()
	{
		p_Game = new JPanel();
		p_Game.setBackground(new Color(0, 0, 0, 100));
		
		p_Game.add(b_CloseFrame);
		p_Main.add(p_Game);
	}
	
	private void setGraphics()
	{
		p_Graphics = new JPanel();
		p_Graphics.setBackground(new Color(0, 0, 0, 100));
		p_Graphics.add(b_CloseFrame);
		p_Main.add(p_Graphics);
	}
	
	private void setControls() throws FileNotFoundException, IOException
	{
		loadSettings();
		p_Controls = new JPanel();
		p_Controls.setLayout(new GridLayout(6,2,6,2));
		p_Controls.setBackground(new Color(0, 0, 0, 100));
		
		l_fwd.setText("Vorw�rts");
		setControl(t_fwd, 'T');
		setControl(l_fwd, 'L');
		l_bwd.setText("R�ckw�rts");
		setControl(t_bwd, 'T');
		setControl(l_bwd, 'L');
		l_lwd.setText("Links");
		setControl(t_lwd, 'T');
		setControl(l_lwd, 'L');
		l_rwd.setText("Rechts");
		setControl(t_rwd, 'T');
		setControl(l_rwd, 'L');
		
		p_Controls.add(l_fwd);
		p_Controls.add(t_fwd);
		p_Controls.add(l_lwd);
		p_Controls.add(t_lwd);
		p_Controls.add(l_bwd);
		p_Controls.add(t_bwd);
		p_Controls.add(l_rwd);
		p_Controls.add(t_rwd);
		p_Main.add(p_Controls, BorderLayout.CENTER);
		p_Main.add(b_CloseFrame, BorderLayout.SOUTH);
	}
	
	private void setSound()
	{
		p_Sound = new JPanel();
		p_Sound.setBackground(new Color(0, 0, 0, 100));
		p_Sound.add(b_CloseFrame);
		p_Main.add(p_Sound);
	}
	
	private void loadSettings() throws IOException, FileNotFoundException
	{
		try
		{
			prop.load(new FileInputStream("prefs.ini"));
			t_fwd.setText(prop.getProperty("Forward"));
			t_bwd.setText(prop.getProperty("Backward"));
			t_lwd.setText(prop.getProperty("Left"));
			t_rwd.setText(prop.getProperty("Right"));
		}
		catch(FileNotFoundException e)
		{
			fos = new FileOutputStream("prefs.ini");
			prop.store(fos, "User Preferences");
		}
		catch(IOException e)
		{
			
		}
	}
	
	private void saveSettings() throws IOException, FileNotFoundException
	{
		try
		{
			fos = new FileOutputStream("prefs.ini");
			//prop.store(fos, "###User Preferences\n###Please do not change.");
			//prop.clear();
			//fos.flush();
			prop.setProperty("Forward", t_fwd.getText(0,1));
			prop.setProperty("Backward", t_bwd.getText(0,1));
			prop.setProperty("Left", t_lwd.getText(0,1));
			prop.setProperty("Right", t_rwd.getText(0,1));
			//prop.store(fos, "\n##Controls");
			//prop.clear();
			//fos.flush();
			prop.setProperty("Resolution", "1920x1080");
			//prop.store(fos, "\n##Graphics");
			prop.store(fos, "###User Preferences\n###Please do not change.");
			prop.clear();
			fos.flush();
			fos.close();
		}
		catch(FileNotFoundException e)
		{
			prop.store(fos, "User Preferences");
		}
		catch(Exception e)
		{
			
		}
	}
	
	public class JTextFieldLimit extends PlainDocument {
		  private int limit;

		  JTextFieldLimit(int limit) {
		   super();
		   this.limit = limit;
		   
		   }
		  
		  public void insertString( int offset, String  str, AttributeSet attr ) throws BadLocationException {
			  if (str == null) return;
		    if ((getLength() + str.length()) <= limit) {
		      super.insertString(offset, str, attr);
		    }
		  }
		}
}

package frames;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.net.Inet4Address;
import java.net.UnknownHostException;

import javax.swing.AbstractAction;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import resources.ResourceLoader;

public class Main extends JFrame {
	
	/**
	 * Serial Number
	 */
	private static final long serialVersionUID = 3478132956775610949L;
	
	JPanel p_main;
	JPanel p_bottom;
	JLabel l_startIcon;
	JButton b_startSinglePlayer;
	JButton b_hostMultiPlayer;
	JButton b_startMultiPlayer;
	JTextField tf_address;
	
	public Main() throws UnknownHostException {
		setSize(475, 370);
		setResizable(false);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		l_startIcon = new JLabel(
				new ImageIcon(ResourceLoader.loadResource("menuicon.jpg")),
				JLabel.CENTER);
		
		tf_address = new JTextField(Inet4Address.getLocalHost().getHostAddress() + ":7777");
		
		b_startSinglePlayer = new JButton(startGame);
		b_startSinglePlayer.setText("Single Player");

		b_startMultiPlayer = new JButton(startGame);
		b_startMultiPlayer.setText("Join Game");

		b_hostMultiPlayer = new JButton(startGame);
		b_hostMultiPlayer.setText("Host Game");
		
		p_bottom = new JPanel();
		p_bottom.setLayout(new BoxLayout(p_bottom, BoxLayout.X_AXIS));
		p_bottom.add(b_startSinglePlayer);
		p_bottom.add(tf_address);
		p_bottom.add(b_hostMultiPlayer);
		p_bottom.add(b_startMultiPlayer);
		
		p_main = new JPanel(new BorderLayout());
		p_main.add(l_startIcon, BorderLayout.CENTER);
		p_main.add(p_bottom, BorderLayout.SOUTH);
		
		add(p_main);
		
		setVisible(true);
	}

	AbstractAction startGame = new AbstractAction() {
		
		private static final long serialVersionUID = -745974447979178323L;

		@Override
		public void actionPerformed(ActionEvent arg0) {
			String address;
			int port;
			if (tf_address.getText().contains(":"))
			{
				String temp[] = tf_address.getText().split(":");
				address = temp[0];
				port = Integer.parseInt(temp[1]);
			}
			else
				return;
			try {
				switch (arg0.getActionCommand()) {
				case "Single Player":
					new GameWindow();
					break;
				case "Host Game":
						new GameWindow(port);
					break;
				case "Join Game":
					new GameWindow(address, port);
					break;
				default:
					new Exception("Unknown command").printStackTrace();
					break;
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	};
	
}

package frames;

import game.Game;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;

import threads.ClientListener;

public class GameWindow extends JFrame {
	private static final long serialVersionUID = -3272694870743440207L;
	public static final byte _con_resyncStatement = 0x8;
	public static final byte _con_valueChangeStatement = 0x1;
	public static final byte _con_pauseStatement = 0x2;
	public static final byte _con_disconnect = 0x3;
	public static final byte _con_message = 0x4;
	public static final byte _con_objectAdded = 0x5;
	public static final byte _con_getNewID = 0x6;
	public static final byte _con_aimLocationChanged = 0x7;
	public static boolean _isMultiplayer = true;
	
	JLabel _display = new JLabel(new ImageIcon());
	Thread _clientlistener;
	public static Socket _hostSocket;
	public static ArrayList<Socket> _clientSockets;
	public static int _port;
	public static boolean _ishost = false;
	
	/**
	 * Host a game on specified port.
	 * @param port The port to listen for clients
	 * @throws IOException 
	 * @see {@link #GameWindow()} {@link #GameWindow(string,int)}
	 */
	public GameWindow(int port) throws IOException
	{
		this(null, port);
	}
	
	/**
	 * Join a game located at specified address via specified port.<br>
	 * <br>
	 * If the address is null, a game will be hosted instead.<br>
	 * If the port is below 1, a single player game will be started instead.
	 * @see {@link #GameWindow()} {@link #GameWindow(int)}
	 * @param address The address to connect to.
	 * @param port The port to use for the connection.
	 * @throws IOException see {@link Socket}
	 */
	public GameWindow(String address, int port) throws IOException
	{
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		GameWindow._port = port;
		addWindowListener(windowCloseHandler);
		_clientSockets = new ArrayList<Socket>();
		
		if (address != null && port > 0) // Joining
		{
			_hostSocket = new Socket(address, port);
		}
		else if (port > 0) // Hosting
		{
			_ishost = true;
			_hostSocket = new Socket();
			_clientlistener = new Thread(new ClientListener(port,_hostSocket,_clientSockets));
			_clientlistener.start();
		}
		else // Single Player
			_isMultiplayer = false;
		
		setSize(400, 400);
		this.add(_display);
		this.setVisible(true);
		Game.Initialize(_display);
		Game.gameStart();
		//new Charactercreation(this);
	}
	
	/**
	 * Starts a single player game.
	 * @throws IOException 
	 * @see {@link #GameWindow(int)} {@link #GameWindow(string,int)}
	 */
	public GameWindow() throws IOException {
		this(null,-1);
	}
	
	WindowAdapter windowCloseHandler = new WindowAdapter() {
		@Override
		public void windowClosing(WindowEvent e) {
			if (_hostSocket != null)
			{
				try {
					_hostSocket.close();
					if (_ishost)
					{
						for (Socket socket : _clientSockets) {
							socket.close();
						}
						_clientlistener.join();
						_ishost = false;
					}
				} catch (IOException | InterruptedException e1) {
					e1.printStackTrace();
				}
			}
			Game.gameStop();
			super.windowClosing(e);
		}
	};
}

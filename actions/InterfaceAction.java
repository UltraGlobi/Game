package actions;

import java.awt.Color;
import java.util.function.Function;

import core.CodeAdditions;
import game.Game;
import game.UserInterface;
import game.UserInterface.UserInterfaceType;

public class InterfaceAction implements Runnable {
	
	private String _name;
	
	public InterfaceAction(String Name) {
		_name = Name;
	}
	
	@Override
	public void run() {
		switch (_name) {
		case "Exit to Windows":
			System.out.println("Closing the application.");
			System.exit(0);
			break;
		case "Start New Game":
			System.out.println("Starting new Game...");
			
			CodeAdditions.getInterface("Startup Menu").setActive(false);
			
			UserInterface gameUI = CodeAdditions.getInterface("Game UI");
			
			if (gameUI == null)
			{
				gameUI = new UserInterface(UserInterfaceType.HUD);
				Game.addGameObject(gameUI);
				gameUI.setActive(true);
			}
			
			break;
		case "Continue":
			System.out.println("Game continued...");

			CodeAdditions.getInterface("HUD").setActive(true);
			
			CodeAdditions.getInterface("Pause Menu").setActive(false);
			
			break;
		default:
			System.out.println("Undefined button-command: '" + _name + "'");
			break;
		}
	}

}

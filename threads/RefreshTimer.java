package threads;

import frames.GameWindow;
import game.Enemy;
import game.Game;
import game.Player;
import game.UserInterface;
import game.UserInterface.UserInterfaceType;
import interfaces.Attributebase;
import interfaces.IRealTimeAction;
import interfaces.IRenderable;
import interfaces.ISynchronisable;

import java.awt.Point;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.TimerTask;
import java.util.function.Function;

import core.CodeAdditions;
import core.GameVars;
import core.GameVars.Controls;
import core.RandomNumberGenerator;

public class RefreshTimer extends TimerTask {

	@SuppressWarnings("unchecked")
	@Override
	public void run() {
		if (!Game._paused) {
			int _level = 1;
			if (Game._Player != null)
				_level = Game._Player.getLevel();
			for (IRenderable gO : (ArrayList<IRenderable>) Game._gameObjects
					.clone()) {
				try {
					if (gO instanceof IRealTimeAction) {
						((IRealTimeAction) gO).Tick();
					}

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			for (Iterator<Object> iterator = Game._gameObjects.iterator(); iterator
					.hasNext();) {
				if (((IRenderable) iterator.next()).canBeRemoved()) {
					iterator.remove();
				}
			}
			if (Game._Player != null) {
				for (GameVars.Controls control : GameVars.Controls.values()) {
					boolean val;
					if (new Point(Game._Player._aimLocation.x
							+ (int) Game._Player.getLocation().x,
							Game._Player._aimLocation.y
									+ (int) Game._Player.getLocation().y)
							.distance(Game._mouselocation) >= ISynchronisable.maximumAimOffsetToSync) {
						if (GameWindow._isMultiplayer) {
							try {
								byte[] command = new byte[Integer.BYTES * 2 + 1];
								ByteBuffer BB = ByteBuffer.wrap(command);
								BB.put(GameWindow._con_aimLocationChanged);
								BB.putInt(Game._mouselocation.x
										- (int) Game._Player.getLocation().x);
								BB.putInt(Game._mouselocation.y
										- (int) Game._Player.getLocation().y);
								GameWindow._hostSocket.getOutputStream().write(
										command);
							} catch (IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						} else
							Game._Player._aimLocation.setLocation(
									Game._mouselocation.x
											- Game._Player.getLocation().x,
									Game._mouselocation.y
											- Game._Player.getLocation().y);
					}
					if (Game._Player.ControlState.get(control) != (val = Game._keystate
							.contains(GameVars.ControlMapping.get(control)))) {
						if (GameWindow._isMultiplayer) {
							try {
								byte[] command = new byte[Integer.BYTES + 2];
								ByteBuffer BB = ByteBuffer.wrap(command);
								BB.put(GameWindow._con_valueChangeStatement);
								BB.putInt(control.ordinal());
								BB.put(val ? (byte) 1 : (byte) 0);
								GameWindow._hostSocket.getOutputStream().write(
										command);
							} catch (IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						} else {
							Game._Player.ControlState.put(control, val);
						}
					}
				}
			}

			if (Game._keystate.contains(GameVars.ControlMapping
					.get(Controls.open_pausemenu))) {
				Game._keystate.remove(Game._keystate
						.indexOf(GameVars.ControlMapping
								.get(Controls.open_pausemenu)));

				UserInterface gameUI = CodeAdditions.getInterface("HUD");

				if (gameUI != null)
					gameUI.setActive(false);

				UserInterface pauseUI = CodeAdditions.getInterface("Pause Menu");

				if (pauseUI == null) {
					pauseUI = new UserInterface(UserInterfaceType.PauseMenu);
					Game.addGameObject(pauseUI);
				}
				pauseUI.setActive(true);
			}

			if (Game._keystate.contains(KeyEvent.VK_V)) {
				Game._keystate.remove(Game._keystate.indexOf(KeyEvent.VK_V));
				Game._showEnemyBars = !Game._showEnemyBars;
			}
			if (Game._keystate.contains(KeyEvent.VK_N)) {
				Game._keystate.remove(Game._keystate.indexOf(KeyEvent.VK_N));
				Game._showEnemyNames = !Game._showEnemyNames;
			}

			if (Game._keystate.contains(KeyEvent.VK_SPACE)) {
				if (GameWindow._ishost || !GameWindow._isMultiplayer) {
					Enemy e = new Enemy(RandomNumberGenerator.Level(_level),
							RandomNumberGenerator.RandomPowerLevel());
					Game._keystate.remove(Game._keystate
							.indexOf(KeyEvent.VK_SPACE));
					if (GameWindow._ishost) {
						try {
							GameWindow._hostSocket.getOutputStream().write(
									CodeAdditions.concatCommand(
											GameWindow._con_objectAdded,
											e.getState()));
						} catch (IOException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					} else {
						Game.addGameObject(e);
					}
				}
			}

			if (Game._keystate.contains(KeyEvent.VK_K)
					&& !GameWindow._isMultiplayer) {
				ArrayList<Attributebase> temp = new ArrayList<Attributebase>();
				Game._keystate.remove(Game._keystate.indexOf(KeyEvent.VK_K));
				for (Object obj : Game._gameObjects) {
					if (obj instanceof Player)
						temp.add((Player) obj);
				}
				Game._gameObjects.retainAll(temp);
			}
		}
	}
}

package threads;

import game.Game;
import interfaces.IRenderable;

import java.awt.Point;
import java.util.ArrayList;
import java.util.TimerTask;

import core.GameVars;

public class RedrawTimer extends TimerTask {
	
	@SuppressWarnings("unchecked")
	@Override
	public void run() {
		//First clear the display, then render all gameObjects.
		//The list is cloned first, it doesn't matter if a single object is updated a frame too late.
		//It is necessary to do this, as otherwise the list might be accessed by multiple threads at once, causing a crash.
		//The GC takes care of the memory-leak this would normally cause.
		
		ArrayList<IRenderable> secondlist;
		try {
			Game._viewOffset = OffsetCalculation();
			secondlist = (ArrayList<IRenderable>)Game._gameObjects.clone();
			Game._g.clearRect(0, 0, Game._Display.getWidth(), Game._Display.getHeight());
			secondlist.forEach(GO -> {if(GO != null)(GO).Draw(Game._g);});
			Game._Display.repaint();
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("i need to sync this...");
		}
	}
	
	private Point OffsetCalculation()
	{
		Point _move = new Point();
		Point _P = new Point();
		Point _offset = new Point();
		
		if (Game._viewOffset != null)
			_offset = Game._viewOffset;
		
		if (Game._Player != null)
		{
			int _x = (int)Game._Player.getLocation().x + _offset.x;
			//int _y = (int)Game.Player.getLocation().y + _offset.y;
			
			if (_x <= GameVars.DistanceToScroll)
				_x -= _x * 2 - GameVars.DistanceToScroll - _offset.x;
			/*else if (_x > Game.Display.getWidth() - GameVars.DistanceToScroll)
				_x += Game.Display.getWidth() - GameVars.DistanceToScroll;*/
			else _x = 0;

			/*if (_y < GameVars.DistanceToScroll)
				_y += GameVars.DistanceToScroll;
			else if (_y > Game.Display.getHeight() - GameVars.DistanceToScroll)
				_y += Game.Display.getHeight() - GameVars.DistanceToScroll;
			else _y = 0;*/
			_move.x = _x/10;
			if (_move.x < 0 && _move.x != -1)
				_move.x --;
			if (_move.x > 0 && _move.x != 1)
				_move.x ++;
			_P.x += _move.x;
			//_P.y = _y;
		}
		return _P;
	}

}

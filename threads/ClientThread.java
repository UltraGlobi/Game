package threads;

import frames.GameWindow;
import game.Game;
import game.Player;
import game.Weapon;
import interfaces.Attributebase;
import interfaces.ISynchronisable;

import java.io.IOException;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.ListIterator;

import core.GameVars.Controls;

public class ClientThread extends Thread {
	Socket _host;

	ArrayList<Byte[]> cmdsToDo = new ArrayList<Byte[]>();
	public static final int ATTEMPTSTOGETID = 30;
	public static final int SLEEPDURATION = 30;
	long newestID = 0;

	public long getNewID() throws IOException, InterruptedException {
		int attempts = 0;
		_host.getOutputStream().write(GameWindow._con_getNewID);
		_host.getOutputStream().flush();
		while (newestID == 0 && attempts < ATTEMPTSTOGETID) {
			System.out
					.println("Attemting to go get a new ID. Attempts remaining: "
							+ (ATTEMPTSTOGETID - attempts));
			sleep(SLEEPDURATION);
			attempts++;
		}
		long ID = newestID;
		newestID = 0;
		return ID;
	}

	/**
	 * Add the Socket for the connection to the Host as argument.
	 * 
	 * @param host
	 *            The Socket which is connected to the Host.
	 */
	public ClientThread(Socket host) {
		_host = host;
	}

	@Override
	public void run() {
		try {

			// Following loop executes until the host-connection
			// is cut.
			while (_host != null && !_host.isClosed()) {
				ISynchronisable objectFromBuffer;

				/*
				 * Prepare a place for the commant, wrap it in a buffer and read
				 * what the host sent.
				 */
				byte[] command = new byte[_host.getInputStream().available()];
				ByteBuffer BB = ByteBuffer.wrap(command);
				_host.getInputStream().read(command);
				/*
				 * if the host sent anything or if there is more of the command
				 * to be executed, run the following loop
				 */
				while (BB.remaining() > 0) {
					// the first byte tells what the command is about.
					switch (BB.get()) {
					// The host closed the server or kicked this client.
					case GameWindow._con_disconnect:
						_host.close(); // close the connection.
						Game.gameStop(); // stop the game.
						return; // Nothing is left to be done.
						// The host wants us to pause the game
					case GameWindow._con_pauseStatement:
						Game.gamePause();
						break;
					// The host sent a resynchronisation.
					case GameWindow._con_resyncStatement:
						// Read everything to the end...
						while (BB.remaining() > 0) {
							// Create an object based on the data the host sent.
							objectFromBuffer = ISynchronisable.fromBuffer(BB);
							/*
							 * Check every object's ID, compare it and: if it
							 * matches, replace it with the new object. if it
							 * doesn't exist, add it to the object-list.
							 */
							if (objectFromBuffer != null) {
								ResyncObject(objectFromBuffer);
							}
						}
						break;
					case GameWindow._con_objectAdded: // The host tells us
														// to add an object.
						// Create the instance of the object from the
						// buffer.
						objectFromBuffer = ISynchronisable.fromBuffer(BB);

						ResyncObject(objectFromBuffer);

						break;
					case GameWindow._con_message: // the server sent a
													// message.
						byte[] message = new byte[BB.remaining()]; // reserve
																	// space
						BB.get(message, BB.position(), BB.remaining()); // read
																		// it
						System.out.println(new String(message)); // write it
						break;
					case GameWindow._con_getNewID: // The server sends a new
													// ID
						if (BB.remaining() == Long.BYTES)
							newestID = BB.getLong();
						else {
							System.err
									.println("Error while attempting to fetch a new Player-ID from the host.");
						}
						break;
					case GameWindow._con_valueChangeStatement:
						objectFromBuffer = ISynchronisable
								.getByID(BB.getLong());
						((Player) objectFromBuffer).ControlState.put(Controls
								.values()[BB.getInt()], BB.get() == 0 ? false
								: true);
						break;
					case GameWindow._con_aimLocationChanged:
						objectFromBuffer = ISynchronisable
								.getByID(BB.getLong());
						((Player) objectFromBuffer)._aimLocation.setLocation(
								BB.getInt(), BB.getInt());
						break;
					default:
						// UNKOWN COMMAND
						System.err
								.println("Received an unknown command from the server...");
						System.err.println(BB.toString()
								+ BB.array().toString());
						break;
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void ResyncObject(ISynchronisable obj) {
		/*
		 * First check if the object has the same ID as the player: if it's the
		 * same, replace the player if it isn't, continue
		 * 
		 * also if the player has a weapon, reassign the weapon to the new
		 * player instance
		 */

		if (Game._Player != null && obj.getID() == Game._Player.getID()) {
			Weapon temp = Game._Player.getWeapon();
			Game._gameObjects.remove(Game._Player);
			Game._Player = (Player) obj;
			Game._gameObjects.add(Game._Player);
			if (temp != null)
				temp.setOwner((Player) obj);
			return;
		}
		/*
		 * Check if an object with the same ID already exists: if it does,
		 * replace it if it doesn't, add it.
		 */
		for (ListIterator<Object> objiterator = Game._gameObjects.listIterator(); objiterator
				.hasNext();) {
			Object object = objiterator.next();
			if (object instanceof ISynchronisable) {
				if (((ISynchronisable) object).getID() == obj.getID()) {
					Weapon temp = null;
					if (object instanceof Attributebase
							&& (temp = ((Attributebase) object)._weapon) != null) {
						if (obj instanceof Attributebase)
							((Attributebase) obj)._weapon = temp;
					}
					objiterator.set(obj);
					break;
				}
				if (!objiterator.hasNext())
					objiterator.add(obj);
			}
		}
	}

}

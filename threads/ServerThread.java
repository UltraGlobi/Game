package threads;

import frames.GameWindow;
import game.Game;
import game.Player;
import game.Weapon;
import interfaces.Attributebase;
import interfaces.ISynchronisable;

import java.io.IOException;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.ListIterator;

import core.CodeAdditions;

public class ServerThread extends Thread {

	ArrayList<Socket> _clients;

	HashMap<Socket, Long> _clientId;

	ArrayList<Byte[]> cmdsToDo = new ArrayList<Byte[]>();

	/**
	 * Create an instance of an ArrayList of Sockets and add it as Argument. The
	 * Thread will then have access to it, so you probably shouldn't overwrite
	 * it afterwards.
	 * 
	 * @param clients
	 *            The reference to the ArrayList of clients.
	 */
	public ServerThread(ArrayList<Socket> clients) {
		this._clients = clients;
		_clientId = new HashMap<>();
	}

	@SuppressWarnings("unchecked")
	@Override
	public void run() {
		byte[] command = null;
		try {

			/*
			 * Following loop executes until all client-connections are cut.
			 */
			while ((_clients != null && _clients.size() > 0)) {
				/*
				 * We use an iterator in case a client disconnects. It will
				 * allow us to remove said client
				 */
				for (ListIterator<Socket> sockiterator = ((ArrayList<Socket>) _clients
						.clone()).listIterator(); sockiterator.hasNext();) {
					Socket sock = sockiterator.next();
					/*
					 * prepare a place for the command, wrap it in a buffer and
					 * read what the client sent
					 */
					command = new byte[sock.getInputStream().available()];
					ByteBuffer BB = ByteBuffer.wrap(command);
					BB.clear();
					sock.getInputStream().read(command);

					/*
					 * if the client sent anything or if there is more of the
					 * command to be executed, run the following loop
					 */
					while (BB.remaining() > 0) {
						/*
						 * the first byte tells what the command is about.
						 */
						switch (BB.get()) {

						// the client disconnected:
						case GameWindow._con_disconnect:
							Disconnection(sock);
							sockiterator.remove();
							break;

						// the client wants to pause the game.
						case GameWindow._con_pauseStatement:
							Pause();
							break;
						// the client requests a resynchronisation.
						case GameWindow._con_resyncStatement:
							Resync(sock);
							break;
						// the client made an object spawn.
						case GameWindow._con_objectAdded:
							AddObject(BB, sock);
							break;
						// the client sent a message
						case GameWindow._con_message:
							SendMessage(command);
							break;
						// the client requests an unused ID
						case GameWindow._con_getNewID:
							SendNewID(sock);
							break;
						// the client changed his state
						case GameWindow._con_valueChangeStatement:
							ValueChanged(BB, sock);
							break;
						case GameWindow._con_aimLocationChanged:
							AimChanged(BB, sock);
							break;
						default:
							System.err
									.println("A Client sent an unknown command...");
							System.err.println(BB.array().toString());
							System.err
									.println("Clearing command to prevent further errors from this...");
							BB.clear();
							break;
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("The Message that caused this error was: "
					+ command.toString());
		}

	}

	/**
	 * Disconnects the socket from the server, and sends other clients a message
	 * of it's disconnection.
	 * 
	 * @param sock
	 *            The socket to disconnect
	 */
	private void Disconnection(Socket sock) {
		try {
			/*
			 * close the connection of the client and remove him to prevent
			 * further exceptions
			 */
			sock.close();

			/*
			 * Here we write a message to every other client, that the current
			 * client disconnected
			 */
			for (Socket sock2 : _clients) {
				sock2.getOutputStream().write(
						CodeAdditions.concatCommand(
								GameWindow._con_message,
								new String(((Player) ISynchronisable
										.getByID(_clientId.get(sock)))
										.getName()
										+ " has disconnected.").getBytes()));
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return;
	}

	/**
	 * Pauses the game for everyone.
	 */
	private void Pause() {
		// Tell everyone to pause their game.
		for (Socket sock2 : _clients) {
			try {
				sock2.getOutputStream().write(GameWindow._con_pauseStatement);
				sock2.getOutputStream().flush();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Sends the state of every object to the client.
	 * 
	 * @param sock
	 *            The client.
	 */
	@SuppressWarnings("unchecked")
	private void Resync(Socket sock) {

		// Preparing an answer and adding the first byte.
		byte[] answerAsByteArray = new byte[1];
		answerAsByteArray[0] = GameWindow._con_resyncStatement;

		/*
		 * Clone all objects to prevent concurrency, and iterate over it.
		 */
		for (Object object : (ArrayList<Object>) Game._gameObjects.clone()) {
			/*
			 * Add every object that is synchronisable to the answer and make
			 * sure to send the weapons after their parents.
			 */
			if (object instanceof Weapon
					&& ((Weapon) object).getOwner() != null)
				continue;

			answerAsByteArray = CodeAdditions.concatCommand(answerAsByteArray,
					((ISynchronisable) object).getState());

			if (object instanceof Attributebase
					&& ((Attributebase) object).getWeapon() != null)
				answerAsByteArray = CodeAdditions.concatCommand(
						answerAsByteArray, ((Attributebase) object).getWeapon()
								.getState());
		}
		try {
			// Writing the answer
			sock.getOutputStream().write(answerAsByteArray);
			sock.getOutputStream().flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Sends every client a message that an object has to be added
	 * 
	 * @param sock
	 *            The socket that sent the command
	 * @param BB
	 *            The ByteBuffer, the object is contained in.
	 */
	private void AddObject(ByteBuffer BB, Socket sock) {

		// Create the object based on the specs the client
		// sent.
		// Attempt to create the object.
		ISynchronisable objectFromBuffer = ISynchronisable.fromBuffer(BB);
		// Verification
		if (objectFromBuffer != null) {
			// If a player was added, we assign his ID right to his Socket.
			if (objectFromBuffer instanceof Player)
				_clientId.put(sock, ((Player) objectFromBuffer).getID());

			// Send every client a message that an object has to be added.
			for (Socket sock2 : _clients) {
				try {
					sock2.getOutputStream().write(
							CodeAdditions.concatCommand(
									GameWindow._con_objectAdded,
									objectFromBuffer.getState()));

					sock2.getOutputStream().flush();
				} catch (IOException e) {
					e.printStackTrace();
				} // End Catch
			} // End For
		} // End If
	} // End Method

	/**
	 * Sends a message to all clients
	 * 
	 * @param command
	 *            The message to be sent.
	 */
	private void SendMessage(byte[] command) {
		// send the message to all the other clients
		for (Socket sock2 : _clients) {
			try {
				sock2.getOutputStream().write(command);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Sends a new ID to the client.
	 * 
	 * @param BB2
	 * @param sock
	 */
	private void SendNewID(Socket sock) {
		byte[] longdata = new byte[Long.BYTES];
		ByteBuffer BB2 = ByteBuffer.wrap(longdata);
		BB2.putLong(ISynchronisable.getRandomtFreeID());
		try {
			sock.getOutputStream().write(
					CodeAdditions.concatCommand(GameWindow._con_getNewID,
							longdata));
			sock.getOutputStream().flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void ValueChanged(ByteBuffer BB, Socket sock) {
		// Inform all clients of this action.
		byte[] command2 = new byte[2 + Integer.BYTES + Long.BYTES];
		ByteBuffer BB2 = ByteBuffer.wrap(command2);
		BB2.put(GameWindow._con_valueChangeStatement);
		// everyone needs to know which player changed
		// state.
		BB2.putLong(_clientId.get(sock));
		BB2.putInt(BB.getInt());
		BB2.put(BB.get());

		for (Socket sock2 : _clients) {
			try {
				sock2.getOutputStream().write(command2);
				sock2.getOutputStream().flush();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private void AimChanged(ByteBuffer BB, Socket sock) {

		// TODO: Find a more efficient way to do this...

		// Inform all clients of this action.
		byte[] command2 = new byte[1 + Integer.BYTES * 2 + Long.BYTES];
		ByteBuffer BB2 = ByteBuffer.wrap(command2);
		BB2.put(GameWindow._con_aimLocationChanged);
		// Everyone needs to know which player changed state.
		BB2.putLong(_clientId.get(sock));
		BB2.putInt(BB.getInt());
		BB2.putInt(BB.getInt());

		for (Socket sock2 : _clients) {
			try {
				sock2.getOutputStream().write(command2);
				sock2.getOutputStream().flush();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
} // End Class

package threads;

import game.Game;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

public class ClientListener implements Runnable {
	
	ServerSocket host;
	Socket hostClient;
	ArrayList<Socket> clients;
	
	public ClientListener(int port, Socket hostClient, ArrayList<Socket> externalClients) throws IOException {
		host = new ServerSocket(port);
		hostClient.connect(host.getLocalSocketAddress());
		this.hostClient = hostClient;
		this.clients = externalClients;
	}
	
	@Override
	public void run() {
		try {
			Socket newClient;
			while(!hostClient.isClosed() && (newClient = host.accept())!=null)
			{
				clients.add(newClient);
			}
			host.close();
			for (Socket client : clients) {
				new PrintWriter(new OutputStreamWriter(client.getOutputStream()), true).println("SERVER: Host '" + Game.getPlayer().getName() + "' Disconnected.");
				client.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}

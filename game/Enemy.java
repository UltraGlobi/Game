package game;

import interfaces.IRealTimeAction;
import interfaces.Attributebase;
import interfaces.IRenderable;
import interfaces.ISynchronisable;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Point2D;
import java.nio.ByteBuffer;
import java.util.ArrayList;

import core.GameVars.*;
import core.GameVars;
import core.RandomNumberGenerator;

public class Enemy extends Attributebase implements IRealTimeAction, ISynchronisable{
	collision col = collision.none;
	long _seed;
	public static final float defaultEnemySize = 15;
	
	float _size;
	
	public Enemy(short Level, PowerLevel Toughness) {
		this(
				-1,
				RandomNumberGenerator.RNG.nextFloat()*Game._Display.getWidth(),
				RandomNumberGenerator.RNG.nextFloat()*Game._Display.getHeight(),
				Level,
				Toughness,
				RandomNumberGenerator.RNG.nextLong(),
				ISynchronisable.getRandomtFreeID());
	}
	
	public Enemy(float health, float x, float y, short Level, PowerLevel Toughness, long seed, long id){
		this._seed = seed;
		RandomNumberGenerator.RNG.setSeed(seed);
		setID(id);
		_level = Level;
		_power = Toughness;
		
		autoInitialize();
		
		for(int i = 1; i < _level; i++)
			autoLevelUp();
		
		String Description = "Lv" + _level + " " + _power.name() + ": ";
		setLocation(x, y);
		
		_size = defaultEnemySize;
		 
		_name = "";
		_name += RandomNumberGenerator.Name(3, 10);
		
		if (_power == PowerLevel.Furniture)
			_name = RandomNumberGenerator.Furniturepresuffix(_name);
		else
			_name = Description + _name;
		recalculate();
		if (health < 0)
			_health = _maxHealth;
		else
			_health = health;
	}
	
	public void Died(Attributebase murderer) {
		while(RandomNumberGenerator.PowerLevelChance(_power))
		{
			Weapon nweap = new Weapon(this);
			//TODO: add code to inform clients of drop.
			Game.addGameObject(nweap);
		}
		super.Died(murderer);
	}
	
	@Override
	public void Draw(Graphics2D Display) {
		// TODO: Replace with Image.
		Color lastcolor = Display.getColor();
		if (Game._showEnemyBars)
		{
			Display.setColor(Color.RED);
			Display.fillRect((int)getOffsetLocation().x -50, (int)(getOffsetLocation().y-_size/2-5), 100, 3);
			Display.setColor(Color.GREEN);
			Display.fillRect((int)getOffsetLocation().x -50, (int)(getOffsetLocation().y-_size/2-5), (int)(getHealth()/getMaxHealth()*100), 3);
			Display.drawRect((int)getOffsetLocation().x -50, (int)(getOffsetLocation().y-_size/2-5), 100, 3);
			Display.setColor(Color.WHITE);
			Display.drawString(String.valueOf(getHealth()), (int)getOffsetLocation().x -47, (int)(getOffsetLocation().y-_size/2-7));
		}
		if (Game._showEnemyNames)
		{
			Display.setColor(Color.RED);
			Display.drawString(_name, (int)getOffsetLocation().x -47, (int)(getOffsetLocation().y-_size/2-20));
		}
		switch (col) {
		case stuck:
			Display.setColor(Color.RED);
			break;
		case none:
			Display.setColor(Color.GREEN);
			break;
		case current:
			Display.setColor(Color.BLUE);
			break;
		case imminend:
			Display.setColor(Color.YELLOW);
			break;
		default:
			break;
		}
		Display.drawOval((int)(getOffsetLocation().x-(_size/2)), (int)(getOffsetLocation().y-(_size/2)), (int)_size, (int)_size);
		/*
		Display.drawString(Location.toString(), Location.x, Location.y);
		Display.drawString(col.name(), Location.x, Location.y + 30);
		*/
		Display.setColor(lastcolor);
	}

	@Override
	public void Tick(){
		@SuppressWarnings("unchecked")
		ArrayList<IRenderable> Environment = (ArrayList<IRenderable>) Game._gameObjects.clone();
		Point2D.Float newLoc = new Point2D.Float();
		IRenderable closest = Closest(Environment);
		Attributebase player = null;

		ArrayList<IRenderable> temp = new ArrayList<IRenderable>();
		
		for (Player gO : IRenderable.Filter(Environment, Player.class))
			temp.add(gO);
		
		player = (Attributebase) Closest(temp);
		
		float distX = player.getLocation().x - getLocation().x;
		float distY = player.getLocation().y - getLocation().y;
		_direction = (float) Math.atan2(distX, distY);
		col = getCollisionState(Environment);
		newLoc.x = (float) (getLocation().x + _movementSpeed / GameVars.getFPS() * Math.sin(_direction));
		newLoc.y = (float) (getLocation().y + _movementSpeed / GameVars.getFPS() * Math.cos(_direction));

		switch (col) {
			case none:
			case current:
				getLocation().x += _movementSpeed / GameVars.getFPS() * Math.sin(_direction);
				getLocation().y += _movementSpeed / GameVars.getFPS() * Math.cos(_direction);
				break;
			case stuck:
				if (closest != null)
				{
					distX = getLocation().x - closest.getLocation().x;
					distY = getLocation().y - closest.getLocation().y;
					_direction = (float) Math.atan2(distX, distY);
					getLocation().x -= (getLocation().distance(closest.getLocation())-_size/2-closest.getSize()/2) * Math.sin(_direction);
					getLocation().y -= (getLocation().distance(closest.getLocation())-_size/2-closest.getSize()/2) * Math.cos(_direction);
				}
			case imminend:
				break;
		}
		if (!hasWeapon())
		{
			if (getLocation().distance(player.getLocation()) <= _size/2 + player.getSize()/2 + _movementSpeed)
			{
				if (_attackCounter >= _meleeSpeed)
				{
						((Player) player).damageMe(this, _meleeDamage, false);
						_attackCounter -= _meleeSpeed;
				}
				else
				{
					_attackCounter++;
				}
			}
			else
				_attackCounter = 0;
		}
	}

	@Override
	public DrawType getDrawType() {
		return DrawType.Circle;
	}

	@Override
	public boolean canBeRemoved() {
		return getHealth() <= 0;
	}

	@Override
	public void setLocation(float x, float y) {
		_location = new Point2D.Float(x,y);
	}

	@Override
	public void setDirection(float value) {
		_direction = value;
	}

	@Override
	public byte[] getState() {
		byte[] state = new byte[getDataSize()];
		ByteBuffer BB = ByteBuffer.wrap(state);
		BB.put((byte) 1); // identification of the class
		BB.putFloat(_health);
		BB.putFloat(getLocation().x);
		BB.putFloat(getLocation().y);
		BB.putShort(_level);
		BB.putInt(_power.ordinal());
		BB.putLong(_seed);
		BB.putLong(getID());
		return state;
	}

	@Override
	public int getDataSize() {
		return
				Byte.BYTES +
				Float.BYTES * 3+
				Short.BYTES +
				Integer.BYTES +
				Long.BYTES * 2;
	}
}

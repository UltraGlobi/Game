package game;

import game.Weapon;
import interfaces.Attributebase;
import interfaces.IRealTimeAction;
import interfaces.IRenderable;
import interfaces.ISynchronisable;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Stroke;
import java.awt.geom.Point2D;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

import core.CodeAdditions;
import core.GameVars;
import core.RandomNumberGenerator;

public class Bullet implements IRealTimeAction, IRenderable, ISynchronisable {
	Attributebase _owner;
	Weapon _firedFrom;
	float _range;
	float _travelled;
	Attributebase _target;
	Attributebase _source;
	List<Bulletbehavior> _behavior = new ArrayList<Bulletbehavior>();
	List<Attributebase> _ignore = new ArrayList<Attributebase>();
	float _width;
	private Point2D.Float _location;
	private double _speed;
	private float _direction;
	private boolean _canBeRemoved;
	private long _id;
	
	@Override
	public void Draw(Graphics2D Display) {
		Stroke Temp = Display.getStroke();
		switch (_firedFrom._weaponType) {
		case Electricity:
			Display.setStroke(new BasicStroke(_width));
			Display.setColor(new Color(150, 120, 255));
			break;
		default:
			Display.setColor(Color.WHITE);
			break;
		}
		if (_behavior.contains(Bulletbehavior.Arcing))
		{
			if (_source != null && _target != null)
				Display.drawLine(
						(int)_source.getLocation().x, (int)_source.getLocation().y,
						(int)_target.getLocation().x, (int)_target.getLocation().y);
		}
		else
		{
			int x2 = (int) (_location.x - Math.cos(getDirection()) * _speed / GameVars.getFPS());
			int y2 = (int) (_location.y - Math.sin(getDirection()) * _speed / GameVars.getFPS());
			Display.drawLine((int)_location.x, (int)_location.y, x2, y2);
		}
		Display.setStroke(Temp);
	}
	
	public Bullet(Attributebase Owner, Weapon Weapon) {
		this(
				RandomNumberGenerator.Angle(
						Owner.getDirection(),
						Weapon._spread),
				Owner.getLocation().x,
				Owner.getLocation().y,
				Owner,
				Weapon,
				ISynchronisable.getRandomtFreeID());
	}
	
	public Bullet(float direction, float x, float y, Attributebase Owner, Weapon Weapon, long id) {
		this._direction = direction;
		this._owner = Owner;
		this._speed = Weapon._bulletSpeed;
		this._range = Owner.getSize()/2 + Weapon._range;
		this._location = new Point2D.Float(x,y);
		this._firedFrom = Weapon;
		this._id = id;
		_ignore.add(Owner);
		switch (Weapon._weaponType) {
		case SniperRifle:
		case Acid:
			_behavior.add(Bulletbehavior.Piercing);
			break;
		case Electricity:
			_behavior.add(Bulletbehavior.Arcing);
			break;
		case Rocket:
			_behavior.add(Bulletbehavior.Explosive);
			break;
		default:
			break;
		}
	}

	@Override
	public String getName() {
		//Death message will be 'X was killed by a bullet fired from Y.'
		return "a bullet fired from " + _owner.getName();
	}

	@SuppressWarnings("unchecked")
	@Override
	public void Tick() {
		ArrayList<IRenderable> Environment = null;
		while (Environment == null && Game._gameObjects != null) {	
			try {
				Environment = (ArrayList<IRenderable>) Game._gameObjects.clone();
			} catch (Exception e) {
				Environment = null;
			}
		}
		Environment.removeAll(_ignore);
		Point2D.Float P1 = (Point2D.Float)_location.clone();
		
		//Default behavior, exclude exotic behaviors
		if (  !(_behavior.contains(Bulletbehavior.Arcing)||
				_behavior.contains(Bulletbehavior.Beam)||
				_behavior.contains(Bulletbehavior.Seeking)))
		{
			_location.x += (float) (Math.cos(_direction) * _speed / GameVars.getFPS());
			_location.y += (float) (Math.sin(_direction) * _speed / GameVars.getFPS());
			_travelled += P1.distance(_location);
		}
		for (IRenderable gO : Environment) {
			if (	gO instanceof Attributebase &&
					gO != _owner &&
					!_ignore.contains(gO) &&
					CodeAdditions.getLineDistance(_location, P1, gO.getLocation()) < gO.getSize()/2)
			{
				Attributebase gO2 = (Attributebase)gO;
				gO2.damageMe(_owner, getDamage(), false);
				if (_behavior.contains(Bulletbehavior.Splitting))
				{
					_ignore.add(gO2);
					try {
						_direction /= 1.1;
						Game._gameObjects.add((IRenderable) this.clone());
						_direction *= 1.21;
						Game._gameObjects.add((IRenderable) this.clone());
					} catch (CloneNotSupportedException e) {
						e.printStackTrace();
					}
				}
				if (!_behavior.contains(Bulletbehavior.Piercing))
					_canBeRemoved = true;
			}
		}
		if (_travelled >= _range)
			_canBeRemoved = true;
		if (_behavior.contains(Bulletbehavior.Arcing))
		{
			if (_target == null)
				_source = _owner;
			else
				_source = _target;
			
			Attributebase gO = ClosestOf(Environment, Attributebase.class);
			if (gO != null && _location.distance(gO.getLocation()) < _range - _travelled)
			{
				_target = gO;
				_ignore.add(gO);
				Environment.remove(gO);
				_travelled += _location.distance(gO.getLocation());
				_location = (Point2D.Float) gO.getLocation().clone();
				_width = 5 / _range * (_range - _travelled);
				gO.damageMe(_owner, getDamage() / _range * (_range-_travelled), false);
			}
			else if (_target != null)
			{
				_travelled = _range;
				_canBeRemoved = true;
			}
			else
				_canBeRemoved = true;
		}
	}

	@Override
	public float getDirection() {
		return _direction;
	}
	
	
	float getDamage() {
		return _owner.getWeapon()._damage;
	}
	
	enum Bulletbehavior{
		Piercing,
		Explosive,
		Arcing,
		Seeking,
		Beam,
		Attracting,
		Splitting
	}

	@Override
	public DrawType getDrawType() {
		return DrawType.Line;
	}

	@Override
	public Color getColor() {
		return _owner.getWeapon().getBulletColor();
	}

	@Override
	public Point2D.Float getLocation() {
		return _location;
	}

	@Override
	public boolean canBeRemoved() {
		return _canBeRemoved;
	}

	@Override
	public void setName(String value) {
		return;
	}

	@Override
	public void setLocation(float x, float y) {
		_location = new Point2D.Float(x,y);
	}

	@Override
	public void setDirection(float value) {
		_direction = value;
	}
	
	public Bullet(float direction, float x, float y, long owner_ID, long weapon_ID, long id) {
		this(direction, x, y, ISynchronisable.getByID(owner_ID), ISynchronisable.getByID(weapon_ID), id);
	}
	
	@Override
	public byte[] getState() {
		byte[] state = new byte[getDataSize()];
		ByteBuffer BB = ByteBuffer.wrap(state);
		BB.put((byte) 3); // identification of the class
		BB.putFloat(_direction);
		BB.putFloat(_location.x);
		BB.putFloat(_location.y);
		BB.putLong(_owner.getID());
		BB.putLong(_firedFrom.getID());
		BB.putLong(_id);
		return state;
	}

	@Override
	public long getID() {
		return _id;
	}

	@Override
	public int getDataSize() {
		return 
				Byte.BYTES +
				Float.BYTES * 3 +
				Long.BYTES * 3;
	}
}
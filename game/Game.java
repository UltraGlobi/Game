package game;

import frames.GameWindow;
import interfaces.Attributebase;
import interfaces.IRenderable;

import java.awt.Graphics2D;
import java.awt.KeyEventDispatcher;
import java.awt.KeyboardFocusManager;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.event.MouseInputAdapter;

import core.CodeAdditions;
import core.GameVars;
import threads.RedrawTimer;
import threads.RefreshTimer;
import threads.ClientThread;
import threads.ServerThread;



public class Game {
	private static Timer _gameTime;	
	private static Timer _frameTime;
	private static TimerTask _gameTick;
	private static TimerTask _frameTick;
	public static ClientThread _syncThread;
	public static ServerThread _hostSyncThread;
	private static BufferedImage _img;
	public static Graphics2D _g;
	public static ArrayList<Integer> _keystate;
	public static Point _mouselocation;
	public static ArrayList<Integer> _buttonstate;
	public static boolean _showEnemyBars = true;
	public static boolean _showEnemyNames = true;
	public static JLabel _Display;
	public static Player _Player;
	public static ArrayList<Object> _gameObjects;
	public static boolean _hasResized = false;
	public static Point _viewOffset;
	private static boolean _running;
	public static boolean _paused = true;
	
	public static void addGameObject(IRenderable gO)
	{
		_gameObjects.add(gO);
	}
	
	public static Attributebase getPlayer()
	{
		if (_Player == null)
			_Player = (Player) CodeAdditions.getFirstMatch(_gameObjects, (gO) -> {return gO instanceof Player;});
		return _Player;
	}
	public static void gameStart()
	{
		if (!_running)
		{
			_gameTime = new Timer();
			_gameTime.scheduleAtFixedRate(_gameTick, 0, 1000/GameVars.getGTPS());
			_frameTime = new Timer();
			_frameTime.schedule(_frameTick, 0, 1000/GameVars.getFPS());
			_running = true;
			_paused = true;
		}
		else
		{
			if (_paused)
				_paused = false;
			else
				System.err.println("Game is already running...");
		}
	}
	public static void gamePause()
	{
		if (_paused)
			System.err.println("Game is already paused...");
		else
			_paused = true;
	}
	public static void gameStop()
	{
		if (_running)
		{
			_gameTime.cancel();
			_frameTime.cancel();
			try {
				if (GameWindow._hostSocket != null)
				{
					GameWindow._hostSocket.getOutputStream().write(GameWindow._con_disconnect);
					GameWindow._hostSocket.close();
					GameWindow._hostSocket = null;
				}
				else
					for (Socket client : GameWindow._clientSockets) {
						client.getOutputStream().write(GameWindow._con_disconnect);
						client.close();
					}
					GameWindow._clientSockets.clear();
				}
			catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			_running = false;
		}
		else
		{
			System.exit(0);
		}
	}
	
	/**
	 * 
	 * @param _display The Surface the game will be rendered on.
	 */
		public static void Initialize(JLabel Label) {
		Game._Display = Label;
		_img = new BufferedImage(_Display.getWidth(), _Display.getHeight(), BufferedImage.TYPE_3BYTE_BGR);
		_Display.setIcon(new ImageIcon(_img));
		_g = _img.createGraphics();
		_g.setRenderingHint(
				RenderingHints.KEY_ANTIALIASING,
				RenderingHints.VALUE_ANTIALIAS_ON);
		GameDispatcher GD = new GameDispatcher();
		_Display.addMouseListener(GD);
		_Display.addMouseMotionListener(GD);
		_keystate = new ArrayList<>();
		_mouselocation = new Point();
		_gameObjects = new ArrayList<>();
		_Display.addComponentListener(new ComponentAdapter() {
			@Override
			public void componentResized(ComponentEvent e) {
				_img = new BufferedImage(_Display.getWidth(), _Display.getHeight(), BufferedImage.TYPE_3BYTE_BGR);
				_Display.setIcon(new ImageIcon(_img));
				_g = _img.createGraphics();
				_g.setRenderingHint(
						RenderingHints.KEY_ANTIALIASING,
						RenderingHints.VALUE_ANTIALIAS_ON);
				_g.setRenderingHint(
						RenderingHints.KEY_INTERPOLATION,
						RenderingHints.VALUE_INTERPOLATION_BICUBIC);
				super.componentResized(e);
			}
		});
		_gameTick = new RefreshTimer();
		
		_frameTick = new RedrawTimer();
		
		
		if (GameWindow._isMultiplayer)
		{
			_syncThread = new ClientThread(GameWindow._hostSocket);
			if (GameWindow._ishost)
			{
				_hostSyncThread = new ServerThread(GameWindow._clientSockets);
				_hostSyncThread.start();
			}
			_syncThread.start();
		}
		
		
		KeyboardFocusManager kfm = KeyboardFocusManager.getCurrentKeyboardFocusManager();
		kfm.addKeyEventDispatcher(new GameDispatcher());
	}
	
	/**
	 * Reads every key-press -down and -up the user causes.
	 * On Down the key pressed is stored if not there already.
	 * On Up the key released is removed if existent.
	 * 
	 * @author Silvan Pfister
	 *
	 */
	private static class GameDispatcher extends MouseInputAdapter implements KeyEventDispatcher{
		
		@Override
		public boolean dispatchKeyEvent(KeyEvent e) {
			switch (e.getID()) {
			case KeyEvent.KEY_PRESSED:
				if (!_keystate.contains(e.getKeyCode()))
					_keystate.add(e.getKeyCode());
				
				break;
			case KeyEvent.KEY_RELEASED:
				if (_keystate.contains(e.getKeyCode()))
					_keystate.remove(_keystate.indexOf(e.getKeyCode()));
				break;
			default:
				break;
			}
			e.getExtendedKeyCode();
			return false;
		}

		@Override
		public void mousePressed(MouseEvent e) {
			if (!_keystate.contains(e.getButton()))
				_keystate.add(e.getButton());
		}

		@Override
		public void mouseReleased(MouseEvent e) {
			if (_keystate.contains(e.getButton()))
				_keystate.remove(_keystate.indexOf(e.getButton()));
		}

		@Override
		public void mouseMoved(MouseEvent e) {
			_mouselocation = e.getPoint();
		}
		@Override
		public void mouseDragged(MouseEvent e) {
			_mouselocation = e.getPoint();
		} 
	}
}

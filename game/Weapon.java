package game;

import interfaces.Attributebase;
import interfaces.IRenderable;
import interfaces.ISynchronisable;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Point2D;
import java.nio.ByteBuffer;
import java.util.ArrayList;

import core.GameVars;
import core.GameVars.ItemRarities;
import core.RandomNumberGenerator;

public class Weapon implements IRenderable, ISynchronisable{
	public static final int _despawnTime = 60;
	public static final float _defaultWeaponSize = 8;
	private float _unOwnedTime;
	boolean _displayDetails = false;
	WeaponType _weaponType;
	ItemRarities _weaponRarity;
	Point2D.Float _location;
	float _range;
	/**
	 * Every Shot fires X pellets. If this Value is 0 or less,
	 * the weapon is considered a melee weapon. 
	 */
	public int _pellets;
	float _spread;
	/**
	 * If this Value is 0 or less,
	 * the weapon will consume ammo without the need to reload.
	 */
	int _magazineSize;
	/**
	 * If this Value is 0, the weapon will neither need to reload, nor consume ammo.
	 * If this Value is negative, the weapon will generate ammo per shot, and upon reloading,
	 * the magazine will be emptied and added to the total ammo.
	 */
	int _ammoPerShot;
	float _reloadSpeed;
	private Attributebase _owner;
	short _level;
	String _name;
	int _bulletSpeed;
	public float _damage;
	public float _attackSpeed;
	float _size = _defaultWeaponSize;
	private long _seed;
	public long _id;
	
	
	public void setOwner(Attributebase Owner)
	{
		if(Owner != null)
		{
			_location = null;
			Owner._weapon = this;
			this._owner = Owner;
		}
	}
	
	public Attributebase getOwner() {
		return _owner;
	}

	public void drop()
	{
		_location = (Point2D.Float) getOwner().getLocation().clone();
		setOwner(null);
	}
	
	@Override
	public Point2D.Float getLocation() {
		if (getOwner() != null)
			return ((IRenderable) getOwner()).getLocation();
		return _location;
	}
	
	public Weapon(Attributebase Dropper)
	{
		this(Dropper.getLevel(), Dropper.getLocation(), RandomNumberGenerator.Enum(WeaponType.class));
	}
	
	
	public Weapon(short Level,Point2D.Float Location){
		this(Level, Location, RandomNumberGenerator.Enum(WeaponType.class));
	}
	
	public Weapon(short Level, Point2D.Float Location, WeaponType AT)
	{
		this(Level, Location, AT, RandomNumberGenerator.RNG.nextLong(), ISynchronisable.getRandomtFreeID(), 0);
	}
	
	public Weapon(short Level,Point2D.Float Location, WeaponType AT, long seed, long id, long ownerID) {
		this._location = Location;
		this._level = Level;
		this._weaponType = AT;
		this._seed = seed;
		this._id = id;
		setOwner(ISynchronisable.getByID(ownerID));
		int Refocus = 1;
		RandomNumberGenerator.RNG.setSeed(seed);
		for (int i = Level; i >= 10; i /= 10)
			Refocus++;
		_name = RandomNumberGenerator.Name(3, 10);
		_bulletSpeed = 200 + Level;
		_range = 200 + 5 * Level;
		_damage = 1.2f * (float)Level;
		_attackSpeed = 1f - 0.02f * (float)Level;
		_magazineSize = 1;
		_reloadSpeed = 10 / (float)(1-0.05*Level);
		_ammoPerShot = 1;
		_spread = (float)Math.PI/4;
		_pellets = 1;
		
		
		switch (_weaponType) {
		case Melee:
			_range *= 0.1;
			_spread *= 8;
			_attackSpeed *= 6;
			_damage *= 10;
			_pellets = 0;
			_magazineSize = 0;
			_ammoPerShot = 0;
			_bulletSpeed = 0;
			_reloadSpeed = _attackSpeed;
			break;
		case Acid:
			_damage *= 0.02;
			_attackSpeed *= 0.01;
			_bulletSpeed *= 0.2;
			_spread *= 6;
			_reloadSpeed *= 2;
			break;
		case AssaultRifle:
			_damage *= 0.6;
			_attackSpeed *= 0.3;
			_spread *= 0.6;
			_bulletSpeed *= 1.4;
			_reloadSpeed *= 0.9;
			break;
		case Electricity:
			_attackSpeed *= 0.006;
			_damage *= 0.05;
			_spread *= 8;
			_bulletSpeed *= 10;
			_reloadSpeed *= 3;
			break;
		case Pistol:
			_attackSpeed *= 0.8;
			_damage *= 1.1;
			_spread *= 0.3;
			_bulletSpeed *= 1.2;
			_reloadSpeed *= 0.7;
			break;
		case Revolver:
			_attackSpeed *= 2;
			_damage *= 3;
			_spread *= 0.4;
			_bulletSpeed *= 1.8;
			_reloadSpeed *= 0.9;
			break;
		case Rocket:
			_attackSpeed *= 6;
			_damage *= 20;
			_spread *= 0.4;
			_bulletSpeed *= 0.7;
			_reloadSpeed *= 8;
			break;
		case Shotgun:
			int sgpellets = 3 + RandomNumberGenerator.LevelScale(Level, 2);
			_damage /= sgpellets * 0.9;
			_pellets = sgpellets;
			_attackSpeed *= 2;
			_spread *= 3;
			_bulletSpeed *= 0.9;
			_reloadSpeed *= 2;
			break;
		case SniperRifle:
			_damage *= 10;
			_attackSpeed *= 4;
			_spread *= 0.1;
			_bulletSpeed *= 3;
			_reloadSpeed *= 5;
			break;
		case SubmachineGun:
			_damage *= 0.1;
			_attackSpeed *= 0.07;
			_spread *= 2;
			_bulletSpeed *= 1.8;
			_reloadSpeed *= 0.9;
			break;
		case Throwable:
			_damage *= 3;
			_attackSpeed *= 1.1;
			_bulletSpeed *= 0.5;
			_reloadSpeed *= 0;
			_spread *= 0.8;
			break;
		default:
			break;
		}
		
		if ((RandomNumberGenerator.Chance(3) && _weaponType != WeaponType.Throwable && _weaponType != WeaponType.Melee))
		{
			_damage *= 2 * _pellets;
			_pellets = 0;
			_range *= 0.3;
			_attackSpeed *= 10;
			_spread *= 8;
		}
		if (RandomNumberGenerator.Chance(6))
		{
			_ammoPerShot = 0;
			_damage *= 0.2;
		}
		
		while (Refocus > 0)
		{
			Refocus--;
			switch (RandomNumberGenerator.RNG.nextInt(9)) {
			case 0:
				_damage += Level;
				break;
			case 1:
				_attackSpeed *= Math.pow(0.95, Level);
				break;
			case 2:
				if (_ammoPerShot > 0)
					_magazineSize += Level;
				else
					_ammoPerShot --;
				break;
			case 3:
				_reloadSpeed *= Math.pow(0.95, Level);
				break;
			case 4:
				if (_pellets > 0)
				{
					if (_ammoPerShot > 1)
						_ammoPerShot --;
					else if (Refocus > 0)
					{
						Refocus--;
						_pellets++;
					}
					else
						Refocus++;
				}
				else if (_weaponType != WeaponType.Melee || _weaponType != WeaponType.Throwable)
					_ammoPerShot--;
				else
					Refocus++;
				break;
			case 5:
				if (_pellets > 0)
					_spread *= Math.pow(0.95, Level);
				else
					_spread *= Math.pow(1.05, Level);
				break;
			case 6:
				if (_pellets > 0)
				{
					_pellets++;
				}
				else
					_damage *= Math.pow(1.05, Level);
				
				_ammoPerShot++;
				break;
			case 7:
				_range *= Math.pow(1.05, Level);
				break;
			case 8:
				_bulletSpeed *= Math.pow(1.05, Level);
				break;
			default:
				break;
			}
		}
		if (_pellets > 1)
			_damage = _damage / _pellets * 1.1f;
	}
	
	@Override
	public void Draw(Graphics2D Display) {
		Display.setColor(getColor());
		if (getOwner() != null)
		{
			_unOwnedTime = 0;
			Display.drawArc(
					(int)(getLocation().x - _range - ((IRenderable)getOwner()).getSize()/2), (int)(getLocation().y - _range - ((IRenderable)getOwner()).getSize()/2),
					(int)(_range * 2 + ((IRenderable)getOwner()).getSize()), (int)(_range * 2 + ((IRenderable)getOwner()).getSize()),
					(int)Math.toDegrees(-(((IRenderable)getOwner()).getDirection() - _spread/2)),
					(int)Math.toDegrees(-_spread));
		}
		else
		{
			_unOwnedTime += 1/GameVars.getFPS();
			Display.drawOval((int)_location.x - 5, (int)_location.y - 5, 10, 10);
			if (_displayDetails)
			{
				Display.drawString("Lv" + _level + " " + _weaponType.name(), _location.x, _location.y);
				Display.drawString("Damage:\t" + _damage + " x " + _pellets, _location.x, _location.y+20);
				Display.drawString("ApS:\t" + 1 / _attackSpeed, _location.x, _location.y+40);
				Display.drawString("Precision:\t" + String.format("%.2g%n", _spread / Math.PI ), _location.x, _location.y+60);
				Display.drawString("Range:\t" + _range, _location.x, _location.y+80);
				Display.drawString("Speed:\t" + _range, _location.x, _location.y+100);
			}
		}
	}
	/**
	 * Various Weapon-types.
	 * @author Silvan Pfister
	 */
	public static enum WeaponType{
		/**
		 * Preferably used for common melee weapons.
		 * If the weapon has an ability related to their own ammo-type,
		 * like ammo regeneration, it will not regenerate any ammo.
		 * Melee weapons with other ammo-types however,
		 * can regenerate their specific kind of ammo, even if it isn't used at all.
		 * This applies to all ammo-related abilities.
		 */
		Melee,
		/**
		 * Weapons with this type of ammo will not share the ammo between them.
		 */
		Throwable,
		/**
		 * As the name implies.
		 */
		Pistol,
		/**
		 * As the name implies.
		 */
		Revolver,
		/**
		 * As the name implies.
		 */
		Shotgun,
		/**
		 * As the name implies.
		 */
		SubmachineGun,
		/**
		 * As the name implies.
		 */
		AssaultRifle,
		/**
		 * As the name implies.
		 */
		SniperRifle,
		/**
		 * As the name implies.
		 */
		Rocket,
		/**
		 * Anything from drills to railguns can use this. 
		 */
		Electricity,
		/**
		 * Yes, acid.
		 */
		Acid,
		//More to come...
	}
	@Override
	public DrawType getDrawType() {
		return DrawType.Circle;
	}

	@Override
	public Color getColor() {
		return GameVars.rarityColorMap.get(_weaponRarity);
	}

	@Override
	public String getName() {
		return _name;
	}

	@Override
	public float getDirection() {// returns the owners direction, or 0, if the weapon isn't owned.
		return getOwner() != null ? ((IRenderable)getOwner()).getDirection() : 0;
	}

	@Override
	public boolean canBeRemoved() {
		return _unOwnedTime >= _despawnTime;
	}

	public Color getBulletColor() {
		switch (_weaponType) {
		case Acid:
			return Color.GREEN;
		case Electricity:
			return Color.BLUE;
		default:
			return Color.WHITE;
		}
	}
	
	
	public ArrayList<Attributebase> InRange()
	{
		return Attributebase.InRange(getLocation(), _range, getDirection(), _spread);
		/*
		float spread;
		float attackrange;
		spread = _spread;
		attackrange = _range + getSize()/2;
		
		Point2D.Float Target = new Point2D.Float(
				getLocation().x + attackrange * (float) Math.cos(getDirection()),
				getLocation().y + attackrange * (float) Math.sin(getDirection()));

		ArrayList<Attributebase> collection = new ArrayList<Attributebase>();
		for (Attributebase gO : Filter((ArrayList<IRenderable>)
				Game.gameObjects.clone(),
				Attributebase.class)) {
			
			float angle = (float) Math.atan2(
					gO.getLocation().y - getLocation().y,
					gO.getLocation().x - getLocation().x);
			
			if (attackrange+gO.getSize()/2 > gO.getLocation().distance(getLocation()) &&
					attackrange+gO.getSize()/2 > gO.getLocation().distance(Target))
			{
				if (
					angle > getDirection() - spread/2 && angle < getDirection() + spread / 2
					||
					gO.getLocation().distance(getLocation()) <  gO.getSize()/2 * 
					Math.abs(Math.tan(Math.PI / 2 + getDirection() - spread/2 - Math.atan2(
							gO.getLocation().y - getLocation().y, 
							gO.getLocation().x - getLocation().x)))
					||
					gO.getLocation().distance(getLocation()) <  gO.getSize()/2 * 
					Math.abs(Math.tan(Math.PI / 2 + getDirection() + spread/2 - Math.atan2(
							gO.getLocation().y - getLocation().y, 
							gO.getLocation().x - getLocation().x)))
					)
					collection.add(gO);
			}
		}
		return collection;
		*/
	}
	
	@Override
	public float getSize() {
		return IRenderable.super.getSize();
	}

	@Override
	public void setName(String value) {
		_name = value;
	}

	@Override
	public void setLocation(float x, float y) {
		_location = new Point2D.Float(x,y);
	}

	@Override
	public void setDirection(float value) {
		return;//Cannot change direction of the player indirectly.
	}

	@Override
	public byte[] getState() {
		byte[] state = new byte[getDataSize()];
		ByteBuffer BB = ByteBuffer.wrap(state);
		BB.put((byte) 2); // identification of the class
		BB.putShort(_level);
		if (getOwner() == null)
		{
			BB.putFloat(_location.x);
			BB.putFloat(_location.y);
		}
		else
		{
			BB.putFloat(0);
			BB.putFloat(0);
		}
		BB.putInt(_weaponType.ordinal());
		BB.putLong(_seed);
		BB.putLong(_id);
		if (getOwner() != null)
			BB.putLong(getOwner().getID());
		else
			BB.putLong(0L);
		return state;
	}

	@Override
	public int getDataSize() {
		return 
				Byte.BYTES +
				Float.BYTES * 2 +
                Integer.BYTES +
                Short.BYTES +
                Long.BYTES * 3;
	}

	@Override
	public long getID() {
		// TODO Auto-generated method stub
		return _id;
	}
}

package game;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.geom.Point2D;
import java.util.ArrayList;

import actions.InterfaceAction;
import interfaces.IRenderable;

public class UserInterface implements IRenderable {

	private String _name;
	private ArrayList<InterfaceElement> _interfaceElements;
	private Boolean _isActive = false;
	private UserInterfaceType _UIT;

	// Implementations
	@Override
	public boolean canBeRemoved() {
		return false;
	}

	@Override
	public void setLocation(float x, float y) {
		return;
	}

	@Override
	public float getDirection() {
		return 0;
	}

	@Override
	public void setDirection(float value) {
		return;
	}

	@Override
	public DrawType getDrawType() {
		return DrawType.Rectangle;
	}

	@Override
	public Color getColor() {
		if (getName() == "HUD")
			return new Color(0, 0, 0, 0);
		return new Color(0xfabadaca);
	}

	@Override
	public String getName() {
		return _name;
	}

	@Override
	public void setName(String value) {
		_name = value;
	}

	@Override
	public Point2D.Float getLocation() {
		if (getName() != "HUD") {
			float x = Game._Display.getWidth() / 2 - getSizeXY().x / 2;
			float y = Game._Display.getHeight() / 2 - getSizeXY().y / 2;
			return new Point2D.Float(x, y);
		} else
			return new Point2D.Float();
	}

	@Override
	public void Draw(Graphics2D Display) {
		if (!isActive())
			return;
		Display.setColor(getColor());
		Display.fillRect((int) getLocation().x, (int) getLocation().y,
				getSizeXY().x, getSizeXY().y);
		for (InterfaceElement iE : _interfaceElements) {
			iE.Draw(Display);
		}
	}

	@Override
	public Point getSizeXY() {
		if (getName() != "HUD") {
			int sizex = Game._g.getFontMetrics().stringWidth(_name) + 8;
			for (InterfaceElement iE : _interfaceElements) {
				if (iE.getSizeXY().x > sizex)
					sizex = iE.getSizeXY().x;
				if (iE.getSize() > sizex)
					sizex = iE.getSizeXY().x;
			}
			return new Point(sizex, _interfaceElements.size() * 22);
		} else
			return new Point(Game._Display.getWidth(),
					Game._Display.getHeight());
	}

	// End of implementations

	public boolean isActive() {
		return _isActive;
	}

	public void setActive(Boolean value) {
		_isActive = value;
		if (_UIT == UserInterfaceType.HUD)
			if (value)
				Game.gameStart();
			else
				Game.gamePause();
	}

	public UserInterface(UserInterfaceType UIT) {
		_interfaceElements = new ArrayList<UserInterface.InterfaceElement>();
		_UIT = UIT;
		switch (UIT) {
		case StartMenu:
			generateStartScreen();
			break;
		case HUD:
			generateHUD();
			break;
		case PauseMenu:
			generatePauseMenu();
			break;
		}
	}

	private void generateStartScreen() {
		_name = "Startup Menu";
		addElement(ElementType.Label, _name);
		addElement(ElementType.Button, "Start New Game");
		addElement(ElementType.Button, "Load Game");
		addElement(ElementType.Button, "Settings");
		addElement(ElementType.Button, "Exit to Windows");

	}

	private void generateHUD() {
		_name = "HUD";
		
		InterfaceElement temp = new InterfaceElement(ElementType.Label, _name,
				this);
		
		temp.setColorAlpha(0);
		
		_interfaceElements.add(temp);
		
		
		
		if (Game._Player == null)
		{
			Player player = new Player();
			player.initialize();
			player.recalculate();
			Game.addGameObject(player);
			Game._Player = player;
		}
	}

	private void generatePauseMenu() {
		_name = "Pause Menu";
		addElement(ElementType.Label, _name);
		addElement(ElementType.Button, "Continue");
		addElement(ElementType.Button, "Save Game");
		addElement(ElementType.Button, "Load Game");
		addElement(ElementType.Button, "Settings");
		addElement(ElementType.Button, "Exit Game");
		addElement(ElementType.Button, "Exit to Windows");
	}

	private void addElement(ElementType ET, String content)
	{
		_interfaceElements.add(new InterfaceElement(ET, content, this));
	}

	
	
	private void addElement(ElementType ET, String content, Point2D.Float location, Point size)
	{
		InterfaceElement iE = new InterfaceElement(ET, content, this);
		iE.setLocation(location.x, location.y);
		iE.setSize(size);
		_interfaceElements.add(iE);
	}
	public enum UserInterfaceType {
		StartMenu, HUD, PauseMenu,
	}

	private class InterfaceElement implements IRenderable {

		private Color _color;

		private String _name;

		private ElementType _eType;

		private UserInterface _parent;

		private Runnable _action;

		private ArrayList<InterfaceElement> _options;

		private boolean _custom = false;

		private Point2D.Float _location;

		private Point _size;

		// Unused implementations
		@Override
		public boolean canBeRemoved() {
			return false;
		}

		@Override
		public Point2D.Float getLocation() {
			if (!_custom) {
				float ysum = 0;
				for (int i = 0; this != _interfaceElements.get(i)
						&& i < _interfaceElements.size(); i++)
					ysum += 22;

				/*
				 * float x = _parent.getLocation().x + _parent.getSizeXY().x / 2
				 * - getSizeXY().x / 2;
				 * 
				 * return new Point2D.Float(x + 1, _parent.getLocation().y + 1 +
				 * ysum);
				 */

				return new Point2D.Float(_parent.getLocation().x + 1,
						_parent.getLocation().y + 1 + ysum);
			} else
				return _location;
		}

		@Override
		public void setLocation(float x, float y) {
			_custom = true;
			_location = new Point2D.Float(x, y);
			if (_size == null)
				_size = new Point(0, 0);
		}

		@Override
		public float getDirection() {
			return 0;
		}

		@Override
		public void setDirection(float value) {
			return;
		}

		@Override
		public DrawType getDrawType() {
			switch (_eType) {
			case Label:
				return DrawType.Rectangle;
			case Bar:
				return DrawType.Rectangle;
			case Button:
				return DrawType.Rectangle;
			case TextArea:
				return DrawType.Rectangle;
			case TextBox:
				return DrawType.Rectangle;
			default:
				return null;
			}
		}

		@Override
		public Color getColor() {
			return _color;
		}

		@SuppressWarnings("unused")
		public void setColor(Color color) {
			_color = color;
		}

		public void setColorAlpha(int alpha) {
			_color = new Color(_color.getRed(), _color.getGreen(),
					_color.getBlue(), alpha);
		}

		@SuppressWarnings("unused")
		public void setColor(byte R, byte G, byte B) {
			_color = new Color(R, G, B);
		}

		@Override
		public String getName() {
			return _name;
		}

		@Override
		public void setName(String value) {
			_name = value;
		}

		@Override
		public Point getSizeXY() {
			if (!_custom)
				return new Point(
						Game._g.getFontMetrics().stringWidth(_name) + 8, 20);
			else
				return _size;
		}

		@SuppressWarnings("unused")
		public void setSize(int size) {
			_size = new Point(size, size);
		}

		@SuppressWarnings("unused")
		public void setSize(Point size) {
			_size = size;
		}

		@SuppressWarnings("unused")
		public void setSize(int x, int y) {
			this._size = new Point(x, y);
		}

		@SuppressWarnings("unused")
		public ElementType getElementType() {
			return _eType;
		}

		public InterfaceElement(ElementType eType, String content,
				UserInterface parent) {
			this(eType, content, parent, null);
		}

		public InterfaceElement(ElementType eType, String content,
				UserInterface parent, ArrayList<String> options) {
			_eType = eType;
			_parent = parent;
			setName(content);
			switch (_eType) {
			case Bar:
				_color = Color.black;
				break;
			case Dropdown:
				_options = new ArrayList<UserInterface.InterfaceElement>();
				for (String string : options)
					_options.add(new InterfaceElement(ElementType.Button,
							string, _parent));
			case Button:
				_color = new Color(0xfaceadad);
				break;
			case Label:
				_color = new Color(0xB000B1E5);
				break;
			default:
				break;
			}
			if (eType == ElementType.Button) {
				_action = new InterfaceAction(content);
			}
		}

		@Override
		public void Draw(Graphics2D Display) {
			boolean hovering = false;
			if (Game._mouselocation.x > getLocation().x
					&& Game._mouselocation.x < getLocation().x
							+ _parent.getSizeXY().x - 2
					&& Game._mouselocation.y > getLocation().y
					&& Game._mouselocation.y < getLocation().y + getSizeXY().y)
				hovering = true;
			Display.setColor(getColor());
			switch (_eType) {
			case Button:
				if (hovering) {
					Display.setColor(getColor().brighter().brighter());
					if (Game._keystate.contains(1)) {
						Game._keystate.remove(Game._keystate.indexOf(1));
						_action.run();
					}
				}
				Display.fillRect((int) _parent.getLocation().x + 1,
						(int) getLocation().y, _parent.getSizeXY().x - 2,
						getSizeXY().y);
				break;
			case Dropdown:
				if (hovering) {
					Display.setColor(getColor().brighter().brighter());
					if (Game._keystate.contains(1)) {
						for (InterfaceElement option : _options) {
							option.Draw(Display);
						}
					}
				}
				Display.fillRect((int) _parent.getLocation().x + 1,
						(int) getLocation().y, _parent.getSizeXY().x - 2,
						getSizeXY().y);
				break;
			case Bar:
			case TextBox:/*
						 * Display.fillRect((int) getLocation().x, (int)
						 * getLocation().y, getSizeXY().x, getSizeXY().y);
						 * break;
						 */
			case Label:/*
						 * Display.fillRect((int) getLocation().x, (int)
						 * getLocation().y, _parent.getSizeXY().x - 2,
						 * getSizeXY().y);
						 */
				Display.fillRect((int) _parent.getLocation().x + 1,
						(int) getLocation().y, _parent.getSizeXY().x - 2,
						getSizeXY().y);
				break;
			case TextArea:
				break;
			default:
				break;
			}
			Display.setColor(getColor().darker().darker());
			Display.drawString(getName(), getLocation().x + 3,
					getLocation().y + 16);
		}
	}

	enum ElementType {
		Label, Button, Bar, TextBox, TextArea, Dropdown, Numeric
	}
}

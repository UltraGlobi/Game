package game;

import interfaces.Attributebase;
import interfaces.IRealTimeAction;
import interfaces.IRenderable;
import interfaces.ISynchronisable;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.geom.Point2D;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;

import core.CodeAdditions;
import core.GameVars;
import core.GameVars.*;
import core.RandomNumberGenerator;

public class Player extends Attributebase implements IRealTimeAction{
	private float _size;
	private boolean _canBeRemoved;
	public static final float _defaultPlayerSize = 15f;
	
	public Point _aimLocation;
	
	
	public HashMap<Controls, Boolean> ControlState = new HashMap<Controls, Boolean>(){
		private static final long serialVersionUID = 5009003581943112901L;
	{
		for (Controls control : Controls.values()) {
			put(control, false);
		}
	}};
	
	public Player() {
		_aimLocation = new Point();
		_power = PowerLevel.Player;
		this.initialize();
		_location = RandomNumberGenerator.Location(0, 0, 500, 500);
		_size = _defaultPlayerSize;
		_health = _maxHealth;
		setID(ISynchronisable.getRandomtFreeID());
	}
	
	@Override
	public void Draw(Graphics2D Display) {
		float spread;
		float attackrange;
		
		//Health Bar
		Display.setColor(Color.RED);
		Display.fillRect(5, 5, 100, 20);
		Display.setColor(Color.GREEN);
		Display.fillRect(5, 5, (int)(getHealth()/getMaxHealth()*100), 20);
		Display.drawRect(5, 5, 100, 20);
		Display.setColor(Color.BLACK);
		Display.drawString(String.valueOf((int)getHealth()) + " / " + String.valueOf((int)getMaxHealth()), 7, 23);
		
		//EXP Bar
		Display.setColor(Color.GRAY);
		Display.fillRect(5, 30, 100, 20);
		Display.setColor(Color.BLUE);
		Display.fillRect(5, 30, (int)((float)getExp()/(float)getExpToLevelup() * 100), 20);
		Display.drawRect(5, 30, 100, 20);
		Display.setColor(Color.WHITE);
		Display.drawString(String.valueOf(getExp()) + " / " + String.valueOf(getExpToLevelup()), 7, 23 + 25);
		
		//Stats
		if (_weapon != null)
		{
			Display.drawString("Weapon: Lv" + _weapon._level + " " + _weapon._name + " (" + _weapon._weaponType.name() + ")", 7, 23 + 50);
			Display.drawString("Attackspeed: " + (float)(1/_weapon._attackSpeed) , 7, 23 + 75);
			Display.drawString("Damage: " + _weapon._damage + " x" + _weapon._pellets, 7, 23 + 100);
			Display.drawString("Precision: " + 1f/_weapon._spread, 7, 23 + 125);
			_weapon.Draw(Display);
		}
		
		//Player
		Display.setColor(Color.WHITE);
		Display.fillOval((int)(getOffsetLocation().x-_size/2) , (int)(getOffsetLocation().y-_size/2) , (int)_size, (int)_size);
		
		if (GameVars._DEBUG_)
		{
			//Enemies in Range
			if (_weapon != null)
			for (Attributebase gO : _weapon.InRange()) {
				Display.drawRect((int)(gO.getOffsetLocation().x - gO.getSize()/2), (int)(gO.getOffsetLocation().y - gO.getSize()/2), (int)(gO.getSize()), (int)(gO.getSize()));
			}
		}

		//Temporary values
		if (_weapon != null)
		{
			spread = _weapon._spread;
			attackrange = _weapon._range + _size/2;
		}
		else
		{
			spread = (float)Math.PI / 4;
			attackrange = this._meleeRange + _size/2;
		}
		
		//Aim Borders
		Display.drawLine(
				(int)getLocation().x,
				(int)getLocation().y,
				(int)(getLocation().x + attackrange * (float) Math.cos(_direction-spread/2)),
				(int)(getLocation().y + attackrange * (float) Math.sin(_direction-spread/2)));
		
		Display.drawLine(
				(int)getLocation().x,
				(int)getLocation().y,
				(int)(getLocation().x + attackrange * (float) Math.cos(_direction+spread/2)),
				(int)(getLocation().y + attackrange * (float) Math.sin(_direction+spread/2)));
		
	}

	/**
	 * This overridden method prevents the despawn of the Player.
	 */
	@Override
	public boolean damageMe(Attributebase attacker, float rawdamage, boolean reflection) {
		if (super.damageMe(attacker, rawdamage, reflection))
		{
			_health = _maxHealth;
			return true;
		}		
		return false;
	}

	@Override
	public DrawType getDrawType() {
		return DrawType.Point;
	}
	
	public boolean canBeRemoved() {
		return _canBeRemoved;
	}

	@Override
	public void setLocation(float x, float y) {
		_location = new Point2D.Float(x,y);
	}

	@Override
	public void setDirection(float value) {
		_direction = value;
	}

	
	
	public Player(
			float direction,
			float x,
			float y,
			float health,
			byte vitality,
			byte strength,
			byte dexterity,
			byte agility,
			byte intelligence,
			byte wisdom,
			long id,
			String name) {
		_aimLocation = new Point();
		_direction = direction;
		_location = new Point2D.Float(x,y);
		_power = PowerLevel.Player;
		_size = _defaultPlayerSize;
		_name = name;
		setAttribute(Attributenames.Vitality, vitality);
		setAttribute(Attributenames.Strength, strength);
		setAttribute(Attributenames.Dexterity, dexterity);
		setAttribute(Attributenames.Agility, agility);
		setAttribute(Attributenames.Intelligence, intelligence);
		setAttribute(Attributenames.Wisdom, wisdom);
		recalculate();
		_health = health;
		setID(id);
	}
	@Override
	public int getDataSize() {
		return 
				Float.BYTES * 4 +
				Byte.BYTES * 7 +
				Long.BYTES +
				Character.BYTES * 16;
	}

	@Override
	public byte[] getState() {
		byte[] state = new byte[getDataSize()];
		ByteBuffer BB = ByteBuffer.wrap(state);
		BB.put((byte) 0); // identification of the class
		BB.putFloat(_direction);
		BB.putFloat(_location.x);
		BB.putFloat(_location.y);
		BB.putFloat(_health);
		BB.put(getVita());
		BB.put(getStr());
		BB.put(getDex());
		BB.put(getAgi());
		BB.put(getInt());
		BB.put(getWis());
		BB.putLong(getID());
		BB.put(getSendableName());
		return state;
	}

	/**
	 * Names need a length of 16. if this condition isn't fulfilled, the synchronisation will fail.
	 * @return returns the name as set of 16 characters as bytes.
	 */
	private byte[] getSendableName()
	{
		byte[] result = _name.substring(0, Math.min(_name.length(), 16)).getBytes();
		result = CodeAdditions.concatCommand(result, new byte[16 - result.length]);
		ByteBuffer BB = ByteBuffer.wrap(result);
		/*changes all 0/zero bytes to 12/twelve ' ' (space) bytes*/
		while (BB.hasRemaining())
			if (BB.get() == 0)
			{
				BB.position(BB.position()-1);
				BB.put(Character.SPACE_SEPARATOR);
			}
		return result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void Tick() 
	{
		ArrayList<IRenderable> Environment = null;
		while (Environment == null && Game._gameObjects != null)
			try {
				Environment = (ArrayList<IRenderable>) Game._gameObjects.clone();
			} catch (Exception e) {
				Environment = null;
			}
			
		Point2D.Float TargetLocation = new Point2D.Float();
		
		float multiplier = _movementSpeed;
		if (ControlState.get(Controls.run))
			multiplier*=1.5;
		if (ControlState.get(Controls.walk))
			multiplier/=1.5;
		
		if (ControlState.get(Controls.move_up))
			TargetLocation.y--;
		if (ControlState.get(Controls.move_down))
			TargetLocation.y++;
		if (ControlState.get(Controls.move_left))
			TargetLocation.x--;
		if (ControlState.get(Controls.move_right))
			TargetLocation.x++;
		_direction = (float) Math.atan2(TargetLocation.y, TargetLocation.x);
		
		if(TargetLocation.x != 0)
			_location.x += multiplier * Math.cos(_direction);
		_location.y += multiplier * Math.sin(_direction);
		_direction = (float) Math.atan2(_aimLocation.getY(), _aimLocation.getX());
		
		attacktimer();
		
		if (ControlState.get(Controls.fire_left))
		{
			attack();
		}
		
		/* Debug for speed
		if (keystate.contains(KeyEvent.VK_UP))
			Speed *= 1.1;
		if (keystate.contains(KeyEvent.VK_DOWN))
			Speed /= 1.1;
*/
		
		
		if (ControlState.get(Controls.pick_up))
		{
			if (this == Game._Player)
				Game._keystate.remove(Game._keystate.indexOf(KeyEvent.VK_E));
			else
				ControlState.put(Controls.pick_up, false);
			
			Weapon closestweapon = null;
			for (IRenderable gO : IRenderable.Filter(Environment, game.Weapon.class)) {
				if (
						((Weapon)gO).getOwner() == null &&
						(closestweapon == null ||
						gO.getLocation().distance(Game._mouselocation) <
						closestweapon.getLocation().distance(Game._mouselocation)))
					closestweapon = (Weapon)gO;
			}
			if (closestweapon != null)
			{
				//Game.gameObjects.remove(closestweapon);
				if (_weapon != null)
				{
					_weapon.drop();
					//Game.addGameObject(_weapon);
				}
				_weapon = closestweapon;
				closestweapon.setOwner(this);
			}
		}
		
		for (IRenderable gO : Environment)
			if (gO instanceof Weapon && gO != _weapon)
				if(Game._mouselocation.distance(gO.getLocation()) < 30)
					((Weapon)gO)._displayDetails = true;
				else
					((Weapon)gO)._displayDetails = false;
	}
	
	
	
}

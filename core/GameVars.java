package core;

import java.awt.Color;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.util.HashMap;
import java.util.Map;

public class GameVars {
	private static int _FPS = 60;
	private static int _GTPS = 60;
	public static final boolean _DEBUG_ = true;
	public static final int DistanceToScroll = 50; //px

	public enum Controls{
		move_up,
		move_down,
		move_left,
		move_right,
		fire_left,
		fire_right,
		run,
		walk,
		pick_up,
		open_pausemenu,
	}
	
	public static HashMap<Controls, Integer> ControlMapping = new HashMap<Controls, Integer>(){
		private static final long serialVersionUID = 8122955878919956819L;
	{
		put(Controls.move_up, KeyEvent.VK_W);
		put(Controls.move_left, KeyEvent.VK_A);
		put(Controls.move_down, KeyEvent.VK_S);
		put(Controls.move_right, KeyEvent.VK_D);
		put(Controls.fire_left, MouseEvent.BUTTON1);
		put(Controls.fire_right, MouseEvent.BUTTON2);
		put(Controls.run, KeyEvent.VK_SHIFT);
		put(Controls.walk, KeyEvent.VK_ALT);
		put(Controls.pick_up, KeyEvent.VK_E);
		put(Controls.open_pausemenu, KeyEvent.VK_ESCAPE);
	}};
	
	public static int getFPS() { return _FPS; }
	public static void setFPS(int value)
	{
		if (value > 0 && value < 600)
			_FPS = value;
	}
	
	public static int getGTPS() { return _GTPS; }
	public static void setGTPS(int value)
	{
		if (value > 0 && value < 600)
			_GTPS = value;
	}

	/**
	 * Describes what type of GameObject and/or how strong an instance is.
	 * @author Silvan Pfister
	 * @see
	 * {@link #Furniture} <br>
	 * {@link #Thrash} <br>
	 * {@link #Common} <br>
	 * {@link #Elite} <br>
	 * {@link #Leader} <br>
	 * {@link #Miniboss} <br>
	 * {@link #Boss} <br>
	 * {@link #Godlike} <br>
	 * {@link #Player}
	 */
	public enum PowerLevel
	{
		/**
		 * Destructable objects which are mostly not even living things. Give almost no reward.
		 * @see PowerLevel
		 */
		Furniture,
		/**
		 * Weak enemies which mostly come in masses. Give little reward, yet more than Furniture.
		 * @see PowerLevel
		 */
		Thrash,
		/**
		 * Normal enemies. Appear often. Not challenging. Therefore not very rewarding.
		 * @see PowerLevel
		 */
		Common,
		/**
		 * Stronger than Normal enemies. A little more rewarding than them.
		 * @see PowerLevel
		 */
		Elite,
		/**
		 * Weaker than Minibosses. Leader of enemy groups. Quite rewarding.
		 * @see PowerLevel
		 */
		Leader,
		/**
		 * One of the most powerful enemies, third after Godlikes and Bosses. Very rewarding.
		 * @see PowerLevel
		 */
		Miniboss,
		/**
		 * Second most powerful enemy-type. Extremely hard and proportionally rewarding.
		 * @see PowerLevel
		 */
		Boss,
		/**
		 * Nearly invincible. Be prepared for multiple levelups and alike.
		 * @see PowerLevel
		 */
		Godlike,
		/**
		 * A Player. Potentionally stronger than Godlike and weaker than Thrash. Grants rewards somewhere between Leader and Miniboss. 
		 * @see PowerLevel
		 */
		Player
	}
	
	public enum Attributenames{
		Vitality,
		Strength,
		Dexterity,
		Agility,
		Intelligence,
		Wisdom
	}
	
	public enum EffectNames{
		Damage_Threshold,
		Damage_Absorption,
		Damage_Reflection,
		Spike_Damage,
	}
	
	public enum ItemRarities{
		Garbage,
		Common,
		Uncommon,
		Rare,
		Epic,
		Legendary,
		Pearlescent
	}
	
	public static final Map<ItemRarities, Color> rarityColorMap =
			new HashMap<ItemRarities, Color>(){
		private static final long serialVersionUID = 2359631951999204530L;
	{
		put(ItemRarities.Garbage, Color.GRAY);
		put(ItemRarities.Common, Color.WHITE);
		put(ItemRarities.Uncommon, Color.GREEN);
		put(ItemRarities.Rare, Color.BLUE);
		put(ItemRarities.Epic, new Color(0xa000ff));
		put(ItemRarities.Legendary, Color.ORANGE);
		put(ItemRarities.Pearlescent, Color.CYAN);
	}};
}

package core;

import game.Game;
import game.UserInterface;

import java.awt.geom.Point2D;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

public class CodeAdditions {
	public static float getLineDistance(Point2D.Float L1, Point2D.Float L2, Point2D.Float P){
		float Len2 = (float) (L1.distance(L2) * L1.distance(L2));
		float T;
		Point2D.Float P2;
		if (Len2 == 0) return (float) P.distance(L1);
		T = ((P.x - L1.x) * (L2.x - L1.x) + (P.y - L1.y) * (L2.y - L1.y)) / Len2;
		if (T < 0) return (float) L1.distance(P); 
		if (T > 1) return (float) L2.distance(P);
		P2 = new Point2D.Float(
				L1.x + T * (L2.x - L1.x),
				L1.y + T * (L2.y - L1.y));
		return (float) P.distance(P2);
	}
	
	public static UserInterface getInterface(String name)
	{
		return (UserInterface) getFirstMatch(Game._gameObjects, new Function<Object, Boolean>() {
		@Override
		public Boolean apply(Object arg0) {
			if (arg0 instanceof UserInterface && ((UserInterface)arg0).getName() == name)
				return true;
			else
				return false; }});
	}
	
	public static byte[] toByteArray(short value){
		return ByteBuffer.allocate(Short.BYTES).putShort(value).array();
	}
	
	public static byte[] toByteArray(int value){
		return ByteBuffer.allocate(Integer.BYTES).putInt(value).array();
	}
	
	public static byte[] toByteArray(long value){
		return ByteBuffer.allocate(Long.BYTES).putLong(value).array();
	}
	
	public static byte[] toByteArray(float value){
		return ByteBuffer.allocate(Float.BYTES).putFloat(value).array();
	}
	
	public static byte[] toByteArray(double value){
		return ByteBuffer.allocate(Double.BYTES).putDouble(value).array();
	}
	
	public static short shortFromByteArray(byte[] value) {
		return ByteBuffer.allocate(Short.BYTES).put(value).getShort(0);
	}
	
	public static int intFromByteArray(byte[] value) {
		return ByteBuffer.allocate(Integer.BYTES).put(value).getInt(0);
	}

	public static long longFromByteArray(byte[] value) {
		return ByteBuffer.allocate(Long.BYTES).put(value).getLong(0);
	}
	
	public static float floatFromByteArray(byte[] value){
		return ByteBuffer.allocate(Float.BYTES).put(value).getFloat(0);
	}
	
	public static double doubleFromByteArray(byte[] value) {
		return ByteBuffer.allocate(Double.BYTES).put(value).getDouble(0);
	}
	
	public static List<Byte> toByteList(short value)
	{
		List<Byte> result = new ArrayList<Byte>();
		for (byte b : ByteBuffer.allocate(Short.BYTES).putShort(value).array()) {
			result.add(b);
		}
		return result;
	}
	
	public static List<Byte> toByteList(int value)
	{
		List<Byte> result = new ArrayList<Byte>();
		for (byte b : ByteBuffer.allocate(Integer.BYTES).putInt(value).array()) {
			result.add(b);
		}
		return result;
	}
	
	public static List<Byte> toByteList(long value)
	{
		List<Byte> result = new ArrayList<Byte>();
		for (byte b : ByteBuffer.allocate(Long.BYTES).putLong(value).array()) {
			result.add(b);
		}
		return result;
	}

	public static List<Byte> toByteList(float value)
	{
		List<Byte> result = new ArrayList<Byte>();
		for (byte b : ByteBuffer.allocate(Float.BYTES).putFloat(value).array()) {
			result.add(b);
		}
		return result;
	}

	public static List<Byte> toByteList(double value)
	{
		List<Byte> result = new ArrayList<Byte>();
		for (byte b : ByteBuffer.allocate(Double.BYTES).putDouble(value).array()) {
			result.add(b);
		}
		return result;
	}
	
	public static List<Byte> byteArrayToList(byte[] array) {
		List<Byte> result = new ArrayList<Byte>();
		for (byte b : array) {
			result.add(b);
		}
		return result;
	}
	
	public static String byteBufferGetString(ByteBuffer bB, int length)
	{
		String result = "";
		for(int i = 0; i < length; i++)
		{
			result += bB.getChar();
		}
		return result;
	}
	
	/**
	 * Compares all objects in objectList and selects the first one that matches the condition.
	 * @param objectList The list to look for a match
	 * @param condition The condition to match
	 * @return The first object that matches the condition.
	 */
	public static <T> T getFirstMatch(List<T> objectList, Function<T,Boolean> condition)
	{
		for (T t : objectList) {
			if (condition.apply(t))
				return t;
		}
		return null;
	}

	
	public static byte[] concatCommand(byte c1, byte[] c2)
	{
		byte[] temp = new byte[c2.length + 1];
		temp[0] = c1;
		for (int i = 0; i < c2.length; i++) {
			temp[1+i] = c2[i];
		}
		return temp;
	}
	
	public static  byte[] concatCommand(byte c1, byte c2)
	{
		byte[] temp = new byte[2];
		temp[0] = c1;
		temp[1] = c2;
		return temp;
	}
	
	public static  byte[] concatCommand(byte[] c1, byte c2)
	{
		byte[] temp = new byte[c1.length + 1];
		for (int i = 0; i < c1.length; i++) {
			temp[i] = c1[i];
		}
		temp[c1.length] = c2;
		return temp;
	}
	
	public static  byte[] concatCommand(byte[] c1, byte[] c2) {
		byte[] temp = new byte[c1.length + c2.length];
		for (int i = 0; i < c1.length; i++) {
			temp[i] = c1[i];
		}
		for (int i = 0; i < c2.length; i++) {
			temp[c1.length + i] = c2[i];
		}
		return temp;
	}
}

package core;


import core.GameVars.PowerLevel;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.Random;

public class RandomNumberGenerator {
	
	/**
	 * Private storage for the Chances, to prevent repetitive initiation in {@link #PowerLevelChances()}.
	 */
	private static TreeMap<Integer, PowerLevel> PowerLevelChances;
	/**
	 * An array determining the chances what enemies will spawn.
		@return a {@link TreeMap} containing the chances for {@link PowerLevel}s.
	 */
	public static TreeMap<Integer,PowerLevel> PowerLevelChances(){
		if (PowerLevelChances == null) //Lazy initiation block
		{
			PowerLevelChances = new TreeMap<>();
			PowerLevelChances.put(2000 , PowerLevel.Furniture);
			PowerLevelChances.put(50000, PowerLevel.Thrash);
			PowerLevelChances.put(10000, PowerLevel.Common);
			PowerLevelChances.put(20000, PowerLevel.Elite);
			PowerLevelChances.put(500  , PowerLevel.Leader);
			PowerLevelChances.put(100  , PowerLevel.Miniboss);
			PowerLevelChances.put(25   , PowerLevel.Boss);
			PowerLevelChances.put(1    , PowerLevel.Godlike);
		}
		return PowerLevelChances;
	}
	
	/**
	 * The Random Number Generator. Your God. Your Definition. Your Life. 
	 */
	public static Random RNG = new Random();
	
	/**
	 * The upward offset for leveled random generation.
	 */
	public static int RandomLevelOffsetU = 3;
	
	/**
	 * The downward offset for leveled random generation.
	 */
	public static int RandomLevelOffsetD = 2;

	/**
	 * Generates an exponential value.
	 * @param Base The base value
	 * @param Multiplier The base multiplier
	 * @param Exponent The exponent
	 * @return a random value between 0 and Base *(Multiplier^Exponent)
	 */
	public static float ScaleExponential(float Base, float Multiplier, int Exponent) { return RNG.nextFloat() * Base * (float)Math.pow(Multiplier, Exponent); }
	
	/**
	 * The use of this method is not very smart. Exponential values equal or higher than 1.5 get too big too fast.
	 * Yet lower values mostly do not affect Integers, and if they do, a lot of data is lost in the process.
	 * If you use this Method, consider changing the value you want to increase to a floating point value first.
	 * @param Base The base value
	 * @param Multiplier The base multiplier
	 * @param Exponent The exponent
	 * @return a random value between 0 and Base *(Multiplier^Exponent)
	 */
	public static int ScaleExponential(int Base, float Multiplier, int Exponent) { return RNG.nextInt(Base * (int)Math.floor(Math.pow(Multiplier, Exponent))); }
	
	/**
	 * Returns a randomized value scaled with the level.
	 * @param Level The level-multiplier
	 * @param Value The value-multiplier
	 * @return Returns a random value ranging from 0 to Level * Value.
	 */
	public static float LevelScale(int Level, float Value){ return RNG.nextFloat()*(float)Level*Value; }
	
	/**
	 * Returns a randomized value scaled with the level.
	 * @param Level The level-multiplier
	 * @param Value The value-multiplier
	 * @return Returns a random value ranging from 0 to Level * Value.
	 */
	public static int LevelScale(int Level, int Value){ return RNG.nextInt(Level*Value); }
	
	/**
	 * Generates a randomized angle.
	 * @param Base The base-direction.
	 * @param Spread The offset the direction may have.
	 * @return a direction ranging from Base - Spread/2 to Base + Spread/2. 
	 */
	public static float Angle(float Base, float Spread) { return Base - Spread/2 + RNG.nextFloat() * Spread; }
	
	/**
	 * Generates a randomized level.
	 * @param Base The base-level.
	 * @return a level ranging from Base - {@link #RandomLevelOffsetD} to Base + {@link #RandomLevelOffsetU} 
	 */
	public static short Level(int Base){ return (short) Math.max(1, Base - RandomLevelOffsetD + RNG.nextInt(RandomLevelOffsetD + RandomLevelOffsetU)); }
	
	/**
	 * Generates a {@link PowerLevel}.
	 * @return a random PowerLevel, excluding Player.
	 * @see {@link PowerLevel}
	 */
	public static PowerLevel RandomPowerLevel()
	{
		int PLCsum = 0;
		int Determinator = 0;
		for (int i : PowerLevelChances().keySet()) {
			PLCsum += i;
		}
		Determinator = RNG.nextInt(PLCsum);
		int PLCsub = 0;
		for (Entry<Integer, PowerLevel> PLCSet : PowerLevelChances.entrySet()) {
			if (PLCSet.getKey() > Determinator - PLCsub)
				return PLCSet.getValue();
			PLCsub += PLCSet.getKey();
		}
		return null;
	}
	
	/**
	 * Selects one value of an Enumeration at random.<br>
	 * Called as in this example:
	 * <code><blockquote>
	 * enum foo{...}<br>
	 * <br>
	 * void foobar(){<br>
	 * foo bar = Randomized.Enum(foo.type);<br>
	 * }
	 * </blockquote></code>
	 * @param T the type of the Enumeration. 
	 * @return a value of the Enumeration.
	 * @throws ClassCastException
	 */
	@SuppressWarnings("unchecked")
	public static <T> T Enum(Class<?> T) { return (T) T.getEnumConstants()[RNG.nextInt(T.getEnumConstants().length)]; }
	
	public static String Name(int minLength, int maxLength)
	{
		String Name = "";
		for (int i = 0; i < RNG.nextInt(maxLength - minLength)+minLength; i++) {
			char addToName = (char) RNG.nextInt(24);
			
			//First character is upper-case, the rest lower-case
			if (i == 0)
				addToName += 65;
			else
				addToName += 97;
			Name += addToName;
		}
		return Name;
	}
	
	public static boolean Chance(float Percentage){ return RNG.nextFloat()*100 < Percentage; }
	
	public static <T> T Elementof(List<T> Collection) {
		if(Collection != null && Collection.size() > 0)
			return Collection.get(RNG.nextInt(Collection.size()));
		else
			return null;
	}
	
	private static ArrayList<String> Furniturenames;
	
	public static String Furniturepresuffix(String Name)
	{
		if (Furniturenames == null) //Lazy initiation block
		{
			Furniturenames = new ArrayList<String>();
			Furniturenames.add("Chair");
			Furniturenames.add("Table");
			Furniturenames.add("Couch");
			Furniturenames.add("Bed");
			Furniturenames.add("Barrel");
			Furniturenames.add("Crate");
		}
		if (Chance(1))
			return "Schrödinger's lively dead Cat";
		if (RNG.nextBoolean())
			return Name + "'s " + Elementof(Furniturenames);
		else
			return Elementof(Furniturenames) + " of " + Name;
	}
	
	public static boolean PowerLevelChance(PowerLevel Base)
	{
		switch (Base) {
		case Furniture:
			return Chance(1f);
		case Thrash:
			return Chance(2.5f);
		case Common:
			return Chance(10f);
		case Elite:
			return Chance(25f);
		case Leader:
			return Chance(40f);
		case Miniboss:
			return Chance(60f);
		case Boss:
			return Chance(80f);
		case Godlike:
			return Chance(95f);
		case Player:
			return false;
		default:
			return false;
		}
	}
	
	/**
	 * Returns a random Location in the rectangle <blockquote><code>
	 * minX|minY maxX|minY <br>
	 * |/////////////////|<br>
	 * |/////////////////|<br>
	 * |/////////////////|<br>
	 * minX|maxY maxX|maxY </code></blockquote>
	 * Pointless demonstrations rock!
	 * @param minX I
	 * @param maxX Don't
	 * @param minY See
	 * @param maxY Any
	 * @return Reason to explain this
	 */
	public static Point2D.Float Location(float minX, float minY, float maxX, float maxY) {
		float X = (maxX - minX) * RNG.nextFloat() + minX;
		float Y = (maxY - minY) * RNG.nextFloat() + minY;
		return new Point2D.Float(X, Y);
	}
}

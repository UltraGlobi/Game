package core;

public class Debug {

	public static void main(String[] args) {
		float f1 = 0.4f;
		byte[] b1 = CodeAdditions.toByteArray(f1);
		float f2 = CodeAdditions.floatFromByteArray(b1);
		System.out.println(f1);
		System.out.println(b1);
		System.out.println(f2);
	}

}
